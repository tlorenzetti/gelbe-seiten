package it.uniroma2.art.gelbeseiten.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public abstract class GSOnto {
	
	public static final String NAMESPACE = "http://schema.gelbeseiten.de/branchen/";
	
	public static final String PREFIX = "";
	
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	/*
	 * Properties
	 */
	
	/*
	 * Instances
	 */
	public static final IRI NAMES;
	
	static {
		ValueFactory vf = SimpleValueFactory.getInstance();
		
		NAMES = vf.createIRI(NAMESPACE, "names"); //scheme
	}

}
