package it.uniroma2.art.gelbeseiten.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public abstract class SKOSThes {
	
	public static final String NAMESPACE = "http://purl.org/iso25964/skos-thes#";
	
	public static final String PREFIX = "iso-thes";
	
	/**
	 * An immutable {@link Namespace} constant that represents the skos-thes namespace.
	 */
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	/*
	 * Properties
	 */
	public static final IRI BROADER_GENERIC;
	public static final IRI BROADER_PARTITIVE;
	public static final IRI PLUS_UF;
	public static final IRI PLUS_USE;
	
	/*
	 * Classes
	 */
	public static final IRI COMPOUND_EQUIVALENCE;
	public static final IRI SPLIT_NON_PREFERRED_TERM;
	
	static {
		ValueFactory vf = SimpleValueFactory.getInstance();

		/*
		 * Properties
		 */
		BROADER_GENERIC = vf.createIRI(NAMESPACE, "broaderGeneric");
		BROADER_PARTITIVE = vf.createIRI(NAMESPACE, "broaderPartitive");
		PLUS_UF = vf.createIRI(NAMESPACE, "plusUF");
		PLUS_USE = vf.createIRI(NAMESPACE, "plusUse");
		
		/*
		 * Classes
		 */
		COMPOUND_EQUIVALENCE = vf.createIRI(NAMESPACE, "CompoundEquivalence");
		SPLIT_NON_PREFERRED_TERM = vf.createIRI(NAMESPACE, "SplitNonPreferredTerm");
		
		
	}
}