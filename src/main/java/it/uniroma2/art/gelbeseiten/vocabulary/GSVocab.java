package it.uniroma2.art.gelbeseiten.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public abstract class GSVocab {
	
	public static final String NAMESPACE = "http://schema.gelbeseiten.de/vocab#";
	
	public static final String PREFIX = "gs";
	
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	/*
	 * Properties
	 */
	public static final IRI ABBREVIATION;
	public static final IRI ACCESS;
	public static final IRI ANTONYM;
	public static final IRI APP_PROP;
	public static final IRI BRAND_PROP;
	public static final IRI BROADER_OTHER;
	public static final IRI BROADER_RESTRICTED;
	public static final IRI CONTROLLED_KEYWORD;
	public static final IRI DIFFERENT_FROM;
	public static final IRI DISPLAY_STATUS;
	public static final IRI ENTRY;
	public static final IRI FACET;
	public static final IRI GRAMMATICAL_STRUCTURE;
	public static final IRI HEADING_TYPE;
	public static final IRI HOURS;
	public static final IRI KEYWORD;
	public static final IRI PRODUCT_OR_SERVICE;
	public static final IRI QUALIFIER;
	public static final IRI TARGET_GROUP;
	public static final IRI WEIGHT;
	
	/*
	 * Classes
	 */
	public static final IRI ACCESS_ENTRY;
	public static final IRI ACCESS_RELATIONSHIP;
	public static final IRI APP_CLASS;
	public static final IRI BRAND_CLASS;
	public static final IRI CLUSTER;
	public static final IRI CONTROLLED_KEYWORD_RELATIONSHIP;
	public static final IRI SUBJECT_GROUP;
	public static final IRI TOPIC;
	
	/*
	 * Instances
	 */
	
	//App
	public static final IRI GS_IPHONE;
	public static final IRI GS_NAHE;
	public static final IRI GS_ONLINE;
	
	//ConceptScheme
	public static final IRI CONTROLLED_KEYWORDS;
	public static final IRI WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
	public static final IRI GRUNDNOMENKLATUR;
	
	//GrammaticalStructure
	public static final IRI TERM_GRAMMAR_AS;
	public static final IRI TERM_GRAMMAR_AV;
	public static final IRI TERM_GRAMMAR_PP;
	public static final IRI TERM_GRAMMAR_SA;
	public static final IRI TERM_GRAMMAR_SV;
	public static final IRI TERM_GRAMMAR_V;
	
	//Heading types
	public static final IRI HEADING_TYPE_BG;
	public static final IRI HEADING_TYPE_BQ;
	public static final IRI HEADING_TYPE_DG;
	public static final IRI HEADING_TYPE_DW;
	public static final IRI HEADING_TYPE_DX;
	public static final IRI HEADING_TYPE_G;
	public static final IRI HEADING_TYPE_K;
	public static final IRI HEADING_TYPE_Q;
	public static final IRI HEADING_TYPE_V;
	public static final IRI HEADING_TYPE_VG;
	public static final IRI HEADING_TYPE_VQ;
	public static final IRI HEADING_TYPE_W;
	public static final IRI HEADING_TYPE_X;
	public static final IRI HEADING_TYPE_XX;
	public static final IRI HEADING_TYPE_Z;
	public static final IRI HEADING_TYPE_Z1;
	public static final IRI HEADING_TYPE_ZB;
	
	//Hours
	public static final IRI HOURS_SPRECHZEITEN;
	public static final IRI HOURS_OFFNUNGSZEITEN;
	
	//TargetGroup
	public static final IRI TARGET_GROUP_A;
	public static final IRI TARGET_GROUP_B;
	public static final IRI TARGET_GROUP_C;
	public static final IRI TARGET_GROUP_N;
	
	//TermFacet
	public static final IRI FACET_B;
	public static final IRI FACET_E;
	public static final IRI FACET_F;
	public static final IRI FACET_G;
	public static final IRI FACET_I;
	public static final IRI FACET_N;
	public static final IRI FACET_P;
	public static final IRI FACET_T;
	public static final IRI FACET_V;
	public static final IRI FACET_X;
	
	//Scheme
	public static final IRI BRANDS;
	
	static {
		ValueFactory vf = SimpleValueFactory.getInstance();
		
		//properties
		ABBREVIATION = vf.createIRI(NAMESPACE, "abbreviation");
		ACCESS = vf.createIRI(NAMESPACE, "access");
		ANTONYM = vf.createIRI(NAMESPACE, "antonym");
		APP_PROP = vf.createIRI(NAMESPACE, "app");
		BRAND_PROP = vf.createIRI(NAMESPACE, "brand");
		BROADER_OTHER = vf.createIRI(NAMESPACE, "broaderOther");
		BROADER_RESTRICTED = vf.createIRI(NAMESPACE, "broaderRestricted");
		CONTROLLED_KEYWORD = vf.createIRI(NAMESPACE, "controlledKeyword");
		DIFFERENT_FROM = vf.createIRI(NAMESPACE, "differentFrom");
		DISPLAY_STATUS = vf.createIRI(NAMESPACE, "displayStatus");
		ENTRY = vf.createIRI(NAMESPACE, "entry");
		FACET = vf.createIRI(NAMESPACE, "facet");
		GRAMMATICAL_STRUCTURE = vf.createIRI(NAMESPACE, "grammaticalStructure");
		HEADING_TYPE = vf.createIRI(NAMESPACE, "headingType");
		HOURS = vf.createIRI(NAMESPACE, "hours");
		KEYWORD = vf.createIRI(NAMESPACE, "keyword");
		PRODUCT_OR_SERVICE = vf.createIRI(NAMESPACE, "productOrService");
		QUALIFIER = vf.createIRI(NAMESPACE, "qualifier");
		TARGET_GROUP = vf.createIRI(NAMESPACE, "targetGroup");
		WEIGHT = vf.createIRI(NAMESPACE, "weight");

		//classes
		ACCESS_ENTRY = vf.createIRI(NAMESPACE, "AccessEntry");
		ACCESS_RELATIONSHIP = vf.createIRI(NAMESPACE, "AccessRelationship");
		APP_CLASS = vf.createIRI(NAMESPACE, "App");
		BRAND_CLASS = vf.createIRI(NAMESPACE, "Brand");
		
		CLUSTER = vf.createIRI(NAMESPACE, "Cluster");
		CONTROLLED_KEYWORD_RELATIONSHIP = vf.createIRI(NAMESPACE, "ControlledKeywordRelationship");
		SUBJECT_GROUP = vf.createIRI(NAMESPACE, "SubjectGroup");
		TOPIC = vf.createIRI(NAMESPACE, "Topic");
		
		CONTROLLED_KEYWORDS = vf.createIRI(NAMESPACE, "ControlledKeywords");
		WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE = vf.createIRI(NAMESPACE, "Wunschbranchen_und_Zugangsbegriffe");
		GRUNDNOMENKLATUR = vf.createIRI(NAMESPACE, "Grundnomenklatur");
		
		GS_IPHONE = vf.createIRI(NAMESPACE, "GS-iPhone");
		GS_NAHE = vf.createIRI(NAMESPACE, "GS-Nähe");
		GS_ONLINE = vf.createIRI(NAMESPACE, "GS-Online");
		
		HEADING_TYPE_BG = vf.createIRI(NAMESPACE, "BG_Online_Grundbranche");
		HEADING_TYPE_BQ = vf.createIRI(NAMESPACE, "BQ_Online_Qualifizierte_Branche");
		HEADING_TYPE_DG = vf.createIRI(NAMESPACE, "DG_Dachbranche_Grundbranche");
		HEADING_TYPE_DW = vf.createIRI(NAMESPACE, "DW_Dachbranche_Wunschbranche");
		HEADING_TYPE_DX = vf.createIRI(NAMESPACE, "DX_Dachbranche_Unerwünschte_Branche");
		HEADING_TYPE_G = vf.createIRI(NAMESPACE, "G_Grundbranche");
		HEADING_TYPE_K = vf.createIRI(NAMESPACE, "K_Keyword");
		HEADING_TYPE_Q = vf.createIRI(NAMESPACE, "Q_Qualifizierte_Branche");
		HEADING_TYPE_V = vf.createIRI(NAMESPACE, "V_Verweis");
		HEADING_TYPE_VG = vf.createIRI(NAMESPACE, "VG_Verweis_oder_Grundbranche");
		HEADING_TYPE_VQ = vf.createIRI(NAMESPACE, "VQ_Verweis_oder_Qualifizierte_Branche");
		HEADING_TYPE_W = vf.createIRI(NAMESPACE, "W_Wunschbranche");
		HEADING_TYPE_X = vf.createIRI(NAMESPACE, "X_Unerwünschte_Branche");
		HEADING_TYPE_XX = vf.createIRI(NAMESPACE, "XX_Fehlschreibung");
		HEADING_TYPE_Z = vf.createIRI(NAMESPACE, "Z_Zugangsbegriff");
		HEADING_TYPE_Z1 = vf.createIRI(NAMESPACE, "Z1_Zugangsbegriff_spezial");
		HEADING_TYPE_ZB = vf.createIRI(NAMESPACE, "ZB_Zugangsbegriff_Benutzersicht");
	
		HOURS_SPRECHZEITEN = vf.createIRI(NAMESPACE, "Sprechzeiten");
		HOURS_OFFNUNGSZEITEN = vf.createIRI(NAMESPACE, "Öffnungszeiten");
		
		TERM_GRAMMAR_AS = vf.createIRI(NAMESPACE, "AS");
		TERM_GRAMMAR_AV = vf.createIRI(NAMESPACE, "AV");
		TERM_GRAMMAR_PP = vf.createIRI(NAMESPACE, "PP");
		TERM_GRAMMAR_SA = vf.createIRI(NAMESPACE, "SA");
		TERM_GRAMMAR_SV = vf.createIRI(NAMESPACE, "SV");
		TERM_GRAMMAR_V = vf.createIRI(NAMESPACE, "V");
		
		TARGET_GROUP_A = vf.createIRI(NAMESPACE, "a_gewerblich");
		TARGET_GROUP_B = vf.createIRI(NAMESPACE, "b_gewerblich_und_privat");
		TARGET_GROUP_C = vf.createIRI(NAMESPACE, "c_privat");
		TARGET_GROUP_N = vf.createIRI(NAMESPACE, "c_privat");//TODO
		
		FACET_B = vf.createIRI(NAMESPACE, "B_Beruf");
		FACET_E = vf.createIRI(NAMESPACE, "E_Ereignis");
		FACET_F = vf.createIRI(NAMESPACE, "F_Fachgebiet");
		FACET_G = vf.createIRI(NAMESPACE, "G_Gegenstand");
		FACET_I = vf.createIRI(NAMESPACE, "I_Institution");
		FACET_N = vf.createIRI(NAMESPACE, "N_Name");
		FACET_P = vf.createIRI(NAMESPACE, "P_Phänomen_Zustand");
		FACET_T = vf.createIRI(NAMESPACE, "T_Tätigkeit");
		FACET_V = vf.createIRI(NAMESPACE, "V_Verfahren");
		FACET_X = vf.createIRI(NAMESPACE, "X_Unklar_oder_Unbekannt");
		
		//Schemes
		BRANDS = vf.createIRI(NAMESPACE, "Brands");
	}

}
