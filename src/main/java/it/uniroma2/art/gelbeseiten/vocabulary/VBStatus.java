package it.uniroma2.art.gelbeseiten.vocabulary;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public abstract class VBStatus {
	
	public static final String NAMESPACE = "http://www.w3.org/2003/06/sw-vocab-status/ns#";
	
	public static final String PREFIX = "vbstatus";
	
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	/*
	 * Properties
	 */
	public static final IRI TERM_STATUS;
	
	static {
		ValueFactory vf = SimpleValueFactory.getInstance();

		/*
		 * Properties
		 */
		TERM_STATUS = vf.createIRI(NAMESPACE, "term_status");
		
	}
}