package it.uniroma2.art.gelbeseiten;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFWriterRegistry;

import it.uniroma2.art.gelbeseiten.exception.SerializationException;
import it.uniroma2.art.gelbeseiten.mapper.RdfToZthesMapper;
import it.uniroma2.art.gelbeseiten.model.Zthes;
import it.uniroma2.art.gelbeseiten.serializer.XmlSerializer;

public class RdfToXml {
	
	private static final String INPUT_RDF_FILE_PROP = "inputRdfFile";
	private static final String OUTPUT_XML_FILE_PROP = "exportXmlFile";

	/**
	 * 
	 * @param args args[0] input rdf file, args[1] output xml file
	 * @throws RDFParseException
	 * @throws RepositoryException
	 * @throws IOException
	 * @throws SerializationException
	 */
	public static void main(String[] args) throws RDFParseException, RepositoryException, IOException, SerializationException {
		
		if (args.length < 1) {
			System.err.println("Usage:\n" + RdfToXml.class.getName() + " <propertiesFilePath>\n");
			System.exit(0);
		}
		
		String propertiesFileName = args[0];
		InputStream input = null;
		try {
			input = new FileInputStream(propertiesFileName);
		} catch (FileNotFoundException e) {
			System.err.println("'"+propertiesFileName+"' not found, or it is not a valid .props file path");
			System.exit(0);
		}
		Properties prop = new Properties();
		prop.load(input);
		
		String inputRdfFilePath = prop.getProperty(INPUT_RDF_FILE_PROP);
		if (inputRdfFilePath == null) {
			System.err.println(INPUT_RDF_FILE_PROP + " property not found, in the input property file "+propertiesFileName);
			System.exit(0);
		}
		File inputFile = new File(inputRdfFilePath);
		if (!inputFile.exists()) {
			System.err.println("\nFile " + inputFile.getAbsolutePath() + " not found.");
			System.exit(0);
		}
		String inputExt = FilenameUtils.getExtension(inputRdfFilePath);
		RDFFormat inputRdfFormat = RDFWriterRegistry.getInstance().getKeys().stream()
				.filter(f -> f.getDefaultFileExtension().equals(inputExt))
				.findAny().orElse(null);
		if (inputRdfFormat == null) {
			System.err.println("\nFile " + inputRdfFormat + " has not a valid RDFFormat extention");
			System.exit(0);
		}
		
		String outputFilePath = prop.getProperty(OUTPUT_XML_FILE_PROP);
		if (outputFilePath == null) {
			System.err.println(OUTPUT_XML_FILE_PROP + " property not found, in the input property file "+propertiesFileName);
			System.exit(0);
		}
		File outputFile = new File(outputFilePath);
		String outputExt = FilenameUtils.getExtension(outputFilePath);
		if (!outputExt.equals("xml")) {
			System.err.println("\nFile " + outputFilePath + " must have .xml extention");
			System.exit(0);
		}
		
		System.out.println("Rdf > Xml");
		
		RdfToZthesMapper mapper = new RdfToZthesMapper();
		mapper.initialize(inputFile);
		Zthes zThes = mapper.map();
		
		System.out.print("Serializing to xml...");
		XmlSerializer serializer = new XmlSerializer();
		serializer.serialize(zThes, outputFile);
		System.out.println("Done");

	}

}
