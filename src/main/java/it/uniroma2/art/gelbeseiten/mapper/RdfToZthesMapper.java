package it.uniroma2.art.gelbeseiten.mapper;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.Query;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.impl.SimpleDataset;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFWriterRegistry;
import org.eclipse.rdf4j.rio.ntriples.NTriplesUtil;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import it.uniroma2.art.gelbeseiten.model.AppName;
import it.uniroma2.art.gelbeseiten.model.ObjectSubtype;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Relation;
import it.uniroma2.art.gelbeseiten.model.RelationType;
import it.uniroma2.art.gelbeseiten.model.TargetGroup;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermFacet;
import it.uniroma2.art.gelbeseiten.model.TermGrammar;
import it.uniroma2.art.gelbeseiten.model.TermNote;
import it.uniroma2.art.gelbeseiten.model.TermType;
import it.uniroma2.art.gelbeseiten.model.Zthes;
import it.uniroma2.art.gelbeseiten.model.TermNote.NoteLabel;
import it.uniroma2.art.gelbeseiten.model.TermStatus;
import it.uniroma2.art.gelbeseiten.vocabulary.GSOnto;
import it.uniroma2.art.gelbeseiten.vocabulary.GSVocab;
import it.uniroma2.art.gelbeseiten.vocabulary.SKOSThes;
import it.uniroma2.art.gelbeseiten.vocabulary.VBStatus;

public class RdfToZthesMapper {
	
	/*
	 * TODO are we sure we want to export the specials as that? in this way, if the IRI changes, they will no more
	 * recognized and exported as specials.
	 * 
	 * TODO setDataset to all query
	 */
	
	private Zthes zThes;
	private RepositoryConnection connection;
	private SimpleValueFactory vf;
	private IRI baseURI;
	
	private Map<String, String> subjectGroupCache; // subjectGroupCode-subjectGroupName
	
	/*
	 * termId should be taken from the prefLabel URI (of the pivot language). In the relations, 
	 * the relation.termId is taken from the URI of the related resource, not from its prefLabel.
	 * This map is useful to keep the mapping between the ID of the resource and the ID of its descriptor
	 * (taken from the pivot prefLabel) and fix the relation.termId at the end of the mapping operations.
	 */
	private Map<String, String> resourceDescriptorIdMap; // resource_ID - termD_ID
	
	public RdfToZthesMapper() {
		Repository rep = new SailRepository(new MemoryStore());
		rep.initialize();
		connection = rep.getConnection();
		connection.setNamespace(GSOnto.NS.getPrefix(), GSOnto.NS.getName());
		
		vf = SimpleValueFactory.getInstance();
		baseURI = vf.createIRI(GSOnto.NAMESPACE);
		
		zThes = new Zthes();
		subjectGroupCache = new HashMap<>();
		resourceDescriptorIdMap = new HashMap<>();
	}
	
	public void initialize(File inputRdfFile) throws RDFParseException, RepositoryException, IOException {
		String ext = FilenameUtils.getExtension(inputRdfFile.getPath());
		RDFFormat dataFormat = RDFWriterRegistry.getInstance().getKeys().stream()
				.filter(f -> f.getDefaultFileExtension().equals(ext))
				.findAny().orElse(null);
		connection.add(inputRdfFile, GSOnto.NS.getName(), dataFormat, baseURI);
	}
	
	public Zthes map() {
		System.out.print("Mapping Topics... ");
		mapTopics();
		System.out.println("Done");
		System.out.print("Mapping Subtopics... ");
		mapSubtopics();
		System.out.println("Done");
		System.out.print("Mapping Headings... ");
		mapHeadings();
		System.out.println("Done");
		System.out.print("Mapping Brands... ");
		mapBrands();
		System.out.println("Done");
		System.out.print("Mapping Keywords... ");
		mapKeywords();
		System.out.println("Done");
		System.out.print("Mapping Names... ");
		mapNames();
		System.out.println("Done");
		
		//there is no mapSpecials since they are already handled as Topic and Subtopic
		
		/* 
		 * fill the attributes of the relations
		 * iterate all over the terms in zThes and all over their relation
		 * retrieve the related term and then fill the attributes of the relation
		 */
		for (Term term : zThes.getTerms()) {
			for (Relation r : term.getRelations()) {
				
				/*
				 * Fix the termId of the relations
				 */
				String fixedRelTermId = resourceDescriptorIdMap.get(r.getTermId());
				if (fixedRelTermId != null) { //relation is toward a termType D term
					r.setTermId(fixedRelTermId);
				}
				
				Term relatedTerm = zThes.getTermById(r.getTermId()); 
				if (relatedTerm == null) {
					System.out.println("Missing rel term: " + term.getTermId() + "(" + term.getObjectType() + ") -" + r.getRelationType() + "-> " + r.getTermId() + "(" + r.getObjectType() + ")");
					continue;
				}
				r.setAppName(relatedTerm.getAppName());
				r.setObjectType(relatedTerm.getObjectType());
				r.setTermId(relatedTerm.getTermId());
				r.setTermLanguage(relatedTerm.getTermLanguage());
				r.setTermName(relatedTerm.getTermName());
				r.setTermQualifier(relatedTerm.getTermQualifier());
				r.setTermType(relatedTerm.getTermType());
			}
		}
		
		//Fix the missing appName to the Subtopic (omitted in topConcept due to redundancy)
		for (Term term : zThes.getTermsByObjectType(ObjectType.T)) {
			for (Relation r : term.getRelations()) {
				if (r.getObjectType() == ObjectType.R) {
					r.setAppName(term.getAppName()); //in relations of subtopic
					zThes.getTermById(r.getTermId()).setAppName(term.getAppName()); //in term
				}
			}
		}
		for (Term term : zThes.getTermsByObjectType(ObjectType.N)) { //special considered topic
			for (Relation r : term.getRelations(RelationType.UB)) {
				if (r.getObjectType() == ObjectType.R || r.getObjectType() == ObjectType.N) {
					r.setAppName(term.getAppName()); //in relations
					zThes.getTermById(r.getTermId()).setAppName(term.getAppName()); //in term
				}
			}
		}
		
		//Fix the BFA relations D->D to D->N
		for (Term term : zThes.getTermsByObjectType(ObjectType.B)) {
			List<Relation> fixedRelationsToN = new ArrayList<>();
			for (Relation bfaRel : term.getRelations(RelationType.BFA)) {
				if (bfaRel.getTermType() == TermType.D) {
					Term termD = zThes.getTermById(bfaRel.getTermId());
					//collect the relations D->N to replace to the relation D->D
					for (Relation bfRel : termD.getRelations(RelationType.BF)) {
						Term termN = zThes.getTermById(bfRel.getTermId());
						Relation relToN = new Relation(RelationType.BFA, termN.getTermId(), termN.getObjectType(), termN.getTermName());
						relToN.setTermLanguage(termN.getTermLanguage());
						relToN.setTermQualifier(termN.getTermQualifier());
						relToN.setTermType(termN.getTermType());
						relToN.setWeight(bfaRel.getWeight());
						fixedRelationsToN.add(relToN);
					}
				}
			}
			//replace the old term relations with the fixed
			List<Relation> fixedRelations = new ArrayList<>();
			//copy the old relations and ignore all the BFA D->D relations
			for (Relation rel : term.getRelations()) {
				if (rel.getRelationType() == RelationType.BFA && rel.getTermType() == TermType.D) {
					continue;
				} else {
					fixedRelations.add(rel);
				}
			}
			//and add all the BFA D->N relations (fixedRelationsToN)
			fixedRelations.addAll(fixedRelationsToN);
			term.setRelation(fixedRelations);
		}
		
		return zThes;
	}
	
	/*
	 * The following mapping methods retrieve all the resources that describe a given object type
	 * (topics, subtopics,...). Then by means a describe query, analyze the relations of the resources
	 * and create the proper Relation(s). Each Relation is left "pending", with most of the attributes missing
	 * (objectType, termName and more), they will be set once all the terms are retrieved 
	 */
	
	private void mapTopics() {
		String query = "SELECT * WHERE { ?topic a " + NTriplesUtil.toNTriplesString(GSVocab.TOPIC) + " }";
		TupleQuery tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		TupleQueryResult result = tq.evaluate();
		
		while (result.hasNext()) {
			IRI topic = (IRI)result.next().getValue("topic");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getDescriptors(topic, ObjectType.T, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, topic, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(topic, ObjectType.T, termIdLabelMap);
			
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//relations only for the pivot
			GraphQueryResult describeResult = describeResource(topic);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(GSVocab.APP_PROP)) {
					pivotTerm.setAppName(AppName.fromString(((IRI)stmt.getObject()).getLocalName()));
				} else if (predicate.equals(SKOS.TOP_CONCEPT_OF) && stmt.getObject().equals(topic)) { //UB relations
					pivotTerm.addRelation(new Relation(RelationType.UB, getTermId((IRI)stmt.getSubject()), null, null));
				}
			}
			
			for (Term t: descriptors) {
				if (t.getTermId().equals("052666")) { //special topic (1)052666 override the ObjectType
					t.setObjectType(ObjectType.N);
				}
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				addTermsWithoutDuplicate(t);
			}
		}
	}
	
	/**
	 * during xml>rdf transformation, appName for subtopic has been omitted for redundancy, so 
	 * during this transformation, appName will not be attached to subtopic
	 */
	private void mapSubtopics() {
		
		String query = "SELECT DISTINCT ?subtopic WHERE { "
			+ "?subtopic " + NTriplesUtil.toNTriplesString(SKOS.TOP_CONCEPT_OF) +  " ?topic . "
			+ "?topic a " + NTriplesUtil.toNTriplesString(GSVocab.TOPIC) +  " . }";
		TupleQuery tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		TupleQueryResult result = tq.evaluate();
		
		while (result.hasNext()) {
			IRI subtopic = (IRI)result.next().getValue("subtopic");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getDescriptors(subtopic, ObjectType.R, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, subtopic, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(subtopic, ObjectType.R, termIdLabelMap);
			
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//relations only for the pivot
			GraphQueryResult describeResult = describeResource(subtopic);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(SKOS.BROADER) && stmt.getObject().equals(subtopic)) { //UB relation
					pivotTerm.addRelation(new Relation(RelationType.UB, getTermId((IRI)stmt.getSubject()), null, null));
				}
			}
			
			for (Term t: descriptors) {
				if (t.getTermId().equals("053516")) { //special subtopic (1)053516 override the ObjectType
					t.setObjectType(ObjectType.N);
				}
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				addTermsWithoutDuplicate(t);
			}
		}
		
	}
	
	private void mapHeadings() {
		String query = "SELECT DISTINCT ?heading WHERE {	\n"
		    + "?heading a " + NTriplesUtil.toNTriplesString(SKOS.CONCEPT) + " .	\n" 
		    + "?heading " + NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME) + " ?scheme .	\n"
		//exclude brands (M)
			+ "MINUS { "
			+ "?heading " + NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME) + " " + NTriplesUtil.toNTriplesString(GSVocab.BRANDS)
			+ " } \n"
		//exclude controlled keywords (Q)
			+ "MINUS { "
			+ "?heading " + NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME) + " " + NTriplesUtil.toNTriplesString(GSVocab.CONTROLLED_KEYWORDS)
			+ " } \n"
		//exclude Names (NA)
			+ "MINUS { "
			+ "?heading " + NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME) + " " + NTriplesUtil.toNTriplesString(GSOnto.NAMES)
			+ " } \n"
		//exclude Topics (R)
			+ "MINUS { "
			+ "?heading " + NTriplesUtil.toNTriplesString(SKOS.TOP_CONCEPT_OF) + " ?scheme"
			+ " } \n"
			+ "}";
		
		TupleQuery tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		TupleQueryResult result = tq.evaluate();
		while (result.hasNext()) {
			IRI heading = (IRI)result.next().getValue("heading");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getDescriptors(heading, ObjectType.B, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, heading, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(heading, ObjectType.B, termIdLabelMap);
			
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//relations only for the pivot
			GraphQueryResult describeResult = describeResource(heading);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(SKOS.BROADER)) {
					if (stmt.getSubject().equals(heading)) { //heading skos:broader ??? => TH
						IRI broader = (IRI) stmt.getObject();
						pivotTerm.addRelation(new Relation(RelationType.TH, getTermId(broader), null, null));
					} else { //??? skos:broader heading => UB
						/* this should be never happen since the skos:broader relation is only between 
						 * heading and subtopic, so heading should never appear as object */
						IRI narrower = (IRI) stmt.getSubject();
						pivotTerm.addRelation(new Relation(RelationType.UB, getTermId(narrower), null, null));
					}
				} else if (predicate.equals(SKOSThes.BROADER_GENERIC)) {
					if (stmt.getSubject().equals(heading)) { //heading skos-thes:broaderGeneric ??? => OBA
						IRI broader = (IRI) stmt.getObject();
						pivotTerm.addRelation(new Relation(RelationType.OBA, getTermId(broader), null, null));
					} else { //??? skos-thes:broaderGeneric heading => UBA
						IRI narrower = (IRI) stmt.getSubject();
						pivotTerm.addRelation(new Relation(RelationType.UBA, getTermId(narrower), null, null));
					}
				} else if (predicate.equals(SKOSThes.BROADER_PARTITIVE)) {
					if (stmt.getSubject().equals(heading)) { //heading skos-thes:broaderPartitive ??? => OBZ
						IRI broader = (IRI) stmt.getObject();
						pivotTerm.addRelation(new Relation(RelationType.OBZ, getTermId(broader), null, null));
					} else { //??? skos-thes:broaderPartitive heading => UBZ
						IRI narrower = (IRI) stmt.getSubject();
						pivotTerm.addRelation(new Relation(RelationType.UBZ, getTermId(narrower), null, null));
					}
				} else if (predicate.equals(GSVocab.BROADER_OTHER)) {
					if (stmt.getSubject().equals(heading)) { //heading gs:broaderOther ??? => OBZ
						IRI broader = (IRI) stmt.getObject();
						pivotTerm.addRelation(new Relation(RelationType.OBZ, getTermId(broader), null, null));
					} else { //??? skos:broaderGeneric heading => UBA
						IRI narrower = (IRI) stmt.getSubject();
						pivotTerm.addRelation(new Relation(RelationType.UBZ, getTermId(narrower), null, null));
					}
				} else if (predicate.equals(GSVocab.BROADER_RESTRICTED)) {
					if (stmt.getSubject().equals(heading)) { //heading gs:broaderOther ??? => OBB
						IRI broader = (IRI) stmt.getObject();
						pivotTerm.addRelation(new Relation(RelationType.OBB, getTermId(broader), null, null));
					} else { //??? gs:broaderOther heading => UBB
						IRI narrower = (IRI) stmt.getSubject();
						pivotTerm.addRelation(new Relation(RelationType.UBB, getTermId(narrower), null, null));
					}
				} else if (predicate.equals(GSVocab.DIFFERENT_FROM)) {
					if (stmt.getSubject().equals(heading)) {
						IRI otherConcept = (IRI) stmt.getObject();
						pivotTerm.addRelation(new Relation(RelationType.NB, getTermId(otherConcept), null, null));
					}
				} else if (predicate.equals(SKOS.RELATED)) { //VB
					if (stmt.getSubject().equals(heading)) {
						pivotTerm.addRelation(new Relation(RelationType.VB, getTermId((IRI)stmt.getObject()), null, null));
					}
				} else if (predicate.equals(SKOS.CLOSE_MATCH)) { //QS
					if (stmt.getSubject().equals(heading)) {
						pivotTerm.addRelation(new Relation(RelationType.QS, getTermId((IRI)stmt.getObject()), null, null));
					}
				} else if (predicate.equals(SKOS.EXACT_MATCH)) { //ID
					if (stmt.getSubject().equals(heading)) {
						pivotTerm.addRelation(new Relation(RelationType.ID, getTermId((IRI)stmt.getObject()), null, null));
					}
				} else if (predicate.equals(GSVocab.ACCESS)) {
					handleAccessRelation(stmt, heading, pivotTerm);
				} else if (predicate.equals(GSVocab.CONTROLLED_KEYWORD)) { //BQK
					IRI controlledKeywordRelation = (IRI) stmt.getObject();
					handleControlledKeywordRelation(controlledKeywordRelation, pivotTerm);
				} else if (predicate.equals(GSVocab.BRAND_PROP)) { //MRK (:brand from concept to :Brand)
					IRI brand = (IRI) stmt.getObject();
					pivotTerm.addRelation(new Relation(RelationType.MRK, getTermId(brand), ObjectType.M, null));
				} else if (predicate.equals(GSVocab.PRODUCT_OR_SERVICE)) { //MRK (:productOrService from :Brand to concept)
					IRI brand = (IRI) stmt.getSubject();
					pivotTerm.addRelation(new Relation(RelationType.MRK, getTermId(brand), ObjectType.M, null));
				} else if (predicate.equals(SKOS.DEFINITION)) {
					String note = ((Literal) stmt.getObject()).stringValue();
					TermNote termNote = new TermNote(note);
					termNote.setLabel(NoteLabel.Definition);
					pivotTerm.addTermNote(termNote);
				} else if (predicate.equals(SKOS.HISTORY_NOTE)) {
					String note = ((Literal) stmt.getObject()).stringValue();
					TermNote termNote = new TermNote(note);
					termNote.setLabel(NoteLabel.History);
					pivotTerm.addTermNote(termNote);
				}
			}
			
			for (Term t: descriptors) {
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				addTermsWithoutDuplicate(t);
			}
		}
		
		//A terms (skos:AccessEntry) 
		query = "SELECT ?accessEntry WHERE { " 
				+ "?accessEntry a " + NTriplesUtil.toNTriplesString(GSVocab.ACCESS_ENTRY) + " . "
				//exclude concepts (already considered in the previous query)
				+ "FILTER NOT EXISTS { ?accessEntry a " + NTriplesUtil.toNTriplesString(SKOS.CONCEPT) + " } }";
		tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		result = tq.evaluate();
		while (result.hasNext()) {
			IRI accessEntry = (IRI)result.next().getValue("accessEntry");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getDescriptors(accessEntry, ObjectType.B, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, accessEntry, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			//this should be useless since A term shouldn't have altLabel
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(accessEntry, ObjectType.B, termIdLabelMap);
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//relations only for the pivot
			GraphQueryResult describeResult = describeResource(accessEntry);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(GSVocab.ENTRY)) {
					handleAccessRelation(stmt, accessEntry, pivotTerm);
				}
			}
			
			for (Term t: descriptors) {
				t.setTermType(TermType.A); //force the termType
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				//this should be useless since A term shouldn't have altLabel
				t.setTermType(TermType.A);
				addTermsWithoutDuplicate(t);
			}
		}
		
		
		//K terms (skos-thes:CompoundEquivalence)
		query = "SELECT ?k WHERE { ?k a " + NTriplesUtil.toNTriplesString(SKOSThes.COMPOUND_EQUIVALENCE) + " } ";
		tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		result = tq.evaluate();
		while (result.hasNext()) {
			IRI compEquivalence = (IRI)result.next().getValue("k");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getTermsK(compEquivalence, ObjectType.B, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, compEquivalence, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			//this should be useless since A term shouldn't have altLabel
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(compEquivalence, ObjectType.B, termIdLabelMap);
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//relations only for the pivot
			GraphQueryResult describeResult = describeResource(compEquivalence);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(SKOSThes.PLUS_USE)) {
					IRI xlabel = (IRI) stmt.getObject();
					if (pivotTerm == null) {
						System.out.println("pivotTerm null");
					}
					pivotTerm.addRelation(new Relation(RelationType.BK, getTermId(xlabel), null, null));
					
					//this seems to produce a NullPointerException since it doesn't find the term related to the given xlabel
//					zThes.getTermById(getTermId(xlabel)).addRelation(new Relation(RelationType.KB, pivotTerm.getTermId(), null, null)); //inverse
				}
			}
			
			for (Term t: descriptors) {
				t.setTermType(TermType.K); //force the termType
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				//this should be useless since K term shouldn't have altLabel
				t.setTermType(TermType.K);
				addTermsWithoutDuplicate(t);
			}
		}
	}
	
	private void mapBrands() {
		String query = "SELECT DISTINCT ?brand WHERE { ?brand " + NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME) + 
				" " + NTriplesUtil.toNTriplesString(GSVocab.BRANDS) + " . }";
		
		TupleQuery tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		TupleQueryResult result = tq.evaluate();
		while (result.hasNext()) {
			IRI brand = (IRI)result.next().getValue("brand");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getDescriptors(brand, ObjectType.M, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, brand, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(brand, ObjectType.M, termIdLabelMap);
			
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//relations only for the pivot
			GraphQueryResult describeResult = describeResource(brand);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(SKOS.RELATED)) { //VB
					if (stmt.getSubject().equals(brand)) {
						pivotTerm.addRelation(new Relation(RelationType.VB, getTermId((IRI)stmt.getObject()), null, null));
					}
				} else if (predicate.equals(GSVocab.BRAND_PROP)) { //PRM (:brand from concept to :Brand)
					IRI productConcept = (IRI) stmt.getSubject();
					pivotTerm.addRelation(new Relation(RelationType.PRM, getTermId(productConcept), null, null));
				} else if (predicate.equals(GSVocab.PRODUCT_OR_SERVICE)) { //PRM (:productOrService from :Brand to concept)
					IRI productConcept = (IRI) stmt.getObject();
					pivotTerm.addRelation(new Relation(RelationType.PRM, getTermId(productConcept), null, null));
				} else if (predicate.equals(SKOSThes.BROADER_GENERIC)) { //OBA, UBA
					if (stmt.getSubject().equals(brand)) { //keyword skos-thes:broaderGeneric ??? => OBA
						IRI broaderBrand = (IRI) stmt.getObject();
						pivotTerm.addRelation(new Relation(RelationType.OBA, getTermId(broaderBrand), null, null));
					} else { //??? skos-thes:broaderGeneric keyword => UBA
						IRI narrowerBrand = (IRI) stmt.getSubject();
						pivotTerm.addRelation(new Relation(RelationType.UBA, getTermId(narrowerBrand), null, null));
					}
				}
			}
			
			for (Term t: descriptors) {
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				addTermsWithoutDuplicate(t);
			}
		}
	}
	
	private void mapKeywords() {
		String query = "SELECT DISTINCT ?keyword WHERE { ?keyword " + NTriplesUtil.toNTriplesString(SKOS.IN_SCHEME) + 
				" " + NTriplesUtil.toNTriplesString(GSVocab.CONTROLLED_KEYWORDS) + " . }";
		
		TupleQuery tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		TupleQueryResult result = tq.evaluate();
		while (result.hasNext()) {
			IRI keyword = (IRI)result.next().getValue("keyword");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getDescriptors(keyword, ObjectType.Q, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, keyword, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(keyword, ObjectType.Q, termIdLabelMap);
			
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//relations only for the pivot
			GraphQueryResult describeResult = describeResource(keyword);
			while (describeResult.hasNext()) { //BS BF QKB VB
				Statement stmt = describeResult.next();
				if (stmt.getPredicate().equals(SKOS.RELATED)) { //VB
					pivotTerm.addRelation(new Relation(RelationType.VB, getTermId((IRI)stmt.getSubject()), null, null));
				} else if (stmt.getPredicate().equals(GSVocab.KEYWORD) && stmt.getObject().equals(keyword)) { //QKB
					IRI keywordRelation = (IRI) stmt.getSubject();
					handleControlledKeywordRelation(keywordRelation, pivotTerm);
				}
			}
			
			for (Term t: descriptors) {
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				addTermsWithoutDuplicate(t);
			}
		}
	}
	
	private void mapNames() {
		String query = "SELECT DISTINCT ?name WHERE { ?name " + NTriplesUtil.toNTriplesString(SKOS.TOP_CONCEPT_OF) +
				" " + NTriplesUtil.toNTriplesString(GSOnto.NAMES) + " . }";
		TupleQuery tq = connection.prepareTupleQuery(query);
		setDataset(tq);
		TupleQueryResult result = tq.evaluate();
		while (result.hasNext()) {
			IRI name = (IRI)result.next().getValue("name");
			
			Map<String, IRI> langLabelMap = new HashMap<>();
			List<Term> descriptors = getDescriptors(name, ObjectType.NA, langLabelMap);
			Term pivotTerm = getPivot(descriptors);
			enrichDescriptors(descriptors, pivotTerm, name, langLabelMap);
			enrichDescriptorNotes(descriptors, pivotTerm, langLabelMap);
			
			Map<String, IRI> termIdLabelMap = new HashMap<>();
			List<Term> nonDescriptors = getNonDescriptors(name, ObjectType.NA, termIdLabelMap);
			
			handleLabelRelations(descriptors, nonDescriptors, termIdLabelMap);
			
			//There are no relations that are no already handled by handleLabelRelations
			
			for (Term t: descriptors) {
				addTermsWithoutDuplicate(t);
			}
			for (Term t: nonDescriptors) {
				addTermsWithoutDuplicate(t);
			}
		}
	}
	
	private void enrichTermAttributes(Term term, IRI resource) {
		GraphQueryResult result = describeResource(resource);
		while (result.hasNext()) {
			Statement stmt = result.next();
			IRI predicate = stmt.getPredicate();;
			if (predicate.equals(DCTERMS.CREATED)) {
				XMLGregorianCalendar xmlDatetime = ((Literal)stmt.getObject()).calendarValue();
				Date date = xmlDatetime.toGregorianCalendar().getTime();
				DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
				term.setTermCreatedDate(formatter.format(date));
			} else if (predicate.equals(DCTERMS.MODIFIED)) {
				XMLGregorianCalendar xmlDatetime = ((Literal)stmt.getObject()).calendarValue();
				Date date = xmlDatetime.toGregorianCalendar().getTime();
				DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
				term.setTermModifiedDate(formatter.format(date));
			} else if (predicate.equals(GSVocab.TARGET_GROUP)) {
				IRI targetGroup = (IRI) stmt.getObject();
				if (targetGroup.equals(GSVocab.TARGET_GROUP_A)) {
					term.setTargetGroup(TargetGroup.A);
				} else if (targetGroup.equals(GSVocab.TARGET_GROUP_B)) {
					term.setTargetGroup(TargetGroup.B);
				} else if (targetGroup.equals(GSVocab.TARGET_GROUP_C)) {
					term.setTargetGroup(TargetGroup.C);
				} else if (targetGroup.equals(GSVocab.TARGET_GROUP_N)) {
					term.setTargetGroup(TargetGroup.N);
				}
			} else if (predicate.equals(GSVocab.HEADING_TYPE)) {
				IRI headingType = (IRI) stmt.getObject();
				if (headingType.equals(GSVocab.HEADING_TYPE_BG)) {
					term.setObjectSubtype(ObjectSubtype.BG);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_BQ)) {
					term.setObjectSubtype(ObjectSubtype.BQ);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_DG)) {
					term.setObjectSubtype(ObjectSubtype.DG);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_DW)) {
					term.setObjectSubtype(ObjectSubtype.DW);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_DX)) {
					term.setObjectSubtype(ObjectSubtype.DX);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_G)) {
					term.setObjectSubtype(ObjectSubtype.G);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_K)) {
					term.setObjectSubtype(ObjectSubtype.K);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_Q)) {
					term.setObjectSubtype(ObjectSubtype.Q);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_V)) {
					term.setObjectSubtype(ObjectSubtype.V);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_VG)) {
					term.setObjectSubtype(ObjectSubtype.VG);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_VQ)) {
					term.setObjectSubtype(ObjectSubtype.VQ);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_W)) {
					term.setObjectSubtype(ObjectSubtype.W);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_X)) {
					term.setObjectSubtype(ObjectSubtype.X);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_XX)) {
					term.setObjectSubtype(ObjectSubtype.XX);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_Z)) {
					term.setObjectSubtype(ObjectSubtype.Z);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_Z1)) {
					term.setObjectSubtype(ObjectSubtype.Z1);
				} else if (headingType.equals(GSVocab.HEADING_TYPE_ZB)) {
					term.setObjectSubtype(ObjectSubtype.ZB);
				} 
			} else if (predicate.equals(GSVocab.APP_PROP)) {
				term.setAppName(AppName.fromString(((IRI)stmt.getObject()).getLocalName()));
			} else if (predicate.equals(GSVocab.HOURS)) {
				IRI hours = (IRI) stmt.getObject();
				if (hours.equals(GSVocab.HOURS_OFFNUNGSZEITEN)) {
					term.setTermHoursValue("");
				} else if (hours.equals(GSVocab.HOURS_SPRECHZEITEN)) {
					term.setTermHoursValue("1");
				}
			} else if (predicate.equals(SKOS.IN_SCHEME) && stmt.getSubject().equals(resource)) {
				enrichClusterOrSubjectGroup((IRI)stmt.getObject(), term);
			}
		}
	}
	
	private void enrichClusterOrSubjectGroup(IRI scheme, Term term) {
		/**
		 * Scheme could represent a subject group or a cluster
		 */
		//check if scheme is a subject group
		if (subjectGroupCache.containsKey(getTermId(scheme))) {
			term.setSubjectGroup(getTermId(scheme));
			term.setSubjectGroupName(subjectGroupCache.get(scheme.getLocalName()));
		} else {
			String askQuery = "ASK { " + NTriplesUtil.toNTriplesString(scheme) + " a " + NTriplesUtil.toNTriplesString(GSVocab.SUBJECT_GROUP) + "}";
			if (connection.prepareBooleanQuery(askQuery).evaluate()) { //is a subject group
				String query = "SELECT ?label WHERE { " 
					+ NTriplesUtil.toNTriplesString(scheme) + " " + NTriplesUtil.toNTriplesString(SKOSXL.PREF_LABEL) + " ?xlabel . " 
					+ "?xlabel " + NTriplesUtil.toNTriplesString(SKOSXL.LITERAL_FORM) + " ?lit . "
					+ "BIND(str(?lit) as ?label) }";
				TupleQueryResult result = connection.prepareTupleQuery(query).evaluate();
				String subjGroupName = result.next().getBinding("label").getValue().stringValue();
				term.setSubjectGroup(getTermId(scheme));
				term.setSubjectGroupName(subjGroupName);
				//store into cache
				subjectGroupCache.put(getTermId(scheme), subjGroupName);
			} else {
				askQuery = "ASK { " + NTriplesUtil.toNTriplesString(scheme) + " a " + NTriplesUtil.toNTriplesString(GSVocab.CLUSTER) + "}";
				if (connection.prepareBooleanQuery(askQuery).evaluate()) { //is a cluster
					term.setCluster(scheme.getLocalName());
				}
			}
		}
	}
	
	private void handleControlledKeywordRelation(IRI controlledKeywordRelation, Term keywordOrHeadingTerm) {
		if (keywordOrHeadingTerm.getObjectType() == ObjectType.Q) { //keyword
			Relation relation = new Relation(RelationType.QKB, null, ObjectType.B, null);
			GraphQueryResult describeResult = describeResource(controlledKeywordRelation);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				if (stmt.getPredicate().equals(GSVocab.CONTROLLED_KEYWORD)) {
					IRI conceptB = (IRI) stmt.getSubject();
					relation.setTermId(getTermId(conceptB));
				} else if (stmt.getPredicate().equals(GSVocab.WEIGHT)) {
					float weight = ((Literal) stmt.getObject()).floatValue();
					relation.setWeight(weight);
				}
			}
			keywordOrHeadingTerm.addRelation(relation);
		} else { //heading
			Relation relation = new Relation(RelationType.BQK, null, ObjectType.Q, null);
			GraphQueryResult describeResult = describeResource(controlledKeywordRelation);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				if (stmt.getPredicate().equals(GSVocab.KEYWORD)) {
					IRI keyword = (IRI) stmt.getObject();
					relation.setTermId(getTermId(keyword));
				} else if (stmt.getPredicate().equals(GSVocab.WEIGHT)) {
					float weight = ((Literal) stmt.getObject()).floatValue();
					relation.setWeight(weight);
				}
			}
			keywordOrHeadingTerm.addRelation(relation);
		}
	}
	
	private void handleAccessRelation(Statement statement, IRI describedResource, Term term) {
		/*
		 * described resource could be a concepts so statement is:
		 * concept gs:access accessRelationship
		 * or an accessEntry, so statement is
		 * accessRelationship gs:entry accessEntry
		 */
		IRI accessRelation = null;
		Relation relation = null;
		if (statement.getSubject().equals(describedResource)) {
			//relation is the object => BFA from D to A (concept to accessEntry)
			accessRelation = (IRI) statement.getObject();
			relation = new Relation(RelationType.BFA, null, null, null);
			GraphQueryResult describeResult = describeResource(accessRelation);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(GSVocab.ENTRY)) {
					IRI accessEntry = (IRI) stmt.getObject();
					relation.setTermId(getTermId(accessEntry));
				} else if (predicate.equals(GSVocab.WEIGHT)) {
					float weight = ((Literal) stmt.getObject()).floatValue();
					relation.setWeight(weight);
				}
			}
		} else if (statement.getObject().equals(describedResource)) {
			//relations is the subject => BSA from A to D (accessEntry to concept)
			accessRelation = (IRI) statement.getSubject();
			relation = new Relation(RelationType.BSA, null, null, null);
			GraphQueryResult describeResult = describeResource(accessRelation);
			while (describeResult.hasNext()) {
				Statement stmt = describeResult.next();
				IRI predicate = stmt.getPredicate();
				if (predicate.equals(GSVocab.ACCESS)) {
					IRI concept = (IRI) stmt.getSubject();
					relation.setTermId(getTermId(concept));
				} else if (predicate.equals(GSVocab.WEIGHT)) {
					float weight = ((Literal) stmt.getObject()).floatValue();
					relation.setWeight(weight);
				}
			}
		}
		term.addRelation(relation);
	}
	
	private void enrichTermFromLabel(Term term, IRI label) {
		GraphQueryResult describeResult = describeResource(label);
		while (describeResult.hasNext()) {
			Statement stmt = describeResult.next();
			IRI predicate = stmt.getPredicate();
			if (predicate.equals(VBStatus.TERM_STATUS)) {
				term.setTermStatus(TermStatus.valueOf(((Literal)stmt.getObject()).getLabel()));
			} else if (predicate.equals(GSVocab.FACET)) {
				IRI facet = (IRI) stmt.getObject();
				if (facet.equals(GSVocab.FACET_B)) {
					term.setTermFacet(TermFacet.B);
				} else if (facet.equals(GSVocab.FACET_E)) {
					term.setTermFacet(TermFacet.E);
				} else if (facet.equals(GSVocab.FACET_F)) {
					term.setTermFacet(TermFacet.F);
				} else if (facet.equals(GSVocab.FACET_G)) {
					term.setTermFacet(TermFacet.G);
				} else if (facet.equals(GSVocab.FACET_I)) {
					term.setTermFacet(TermFacet.I);
				} else if (facet.equals(GSVocab.FACET_N)) {
					term.setTermFacet(TermFacet.N);
				} else if (facet.equals(GSVocab.FACET_P)) {
					term.setTermFacet(TermFacet.P);
				} else if (facet.equals(GSVocab.FACET_T)) {
					term.setTermFacet(TermFacet.T);
				} else if (facet.equals(GSVocab.FACET_V)) {
					term.setTermFacet(TermFacet.V);
				} else if (facet.equals(GSVocab.FACET_X)) {
					term.setTermFacet(TermFacet.X);
				}
			} else if (predicate.equals(GSVocab.QUALIFIER)) {
				term.setTermQualifier(((Literal)stmt.getObject()).getLabel());
			} else if (predicate.equals(GSVocab.DISPLAY_STATUS)) {
				boolean dispStatus = ((Literal) stmt.getObject()).booleanValue();
				if (dispStatus) {
					term.setTermDisplayStatus("1");
				}
			} else if (predicate.equals(GSVocab.GRAMMATICAL_STRUCTURE)) {
				IRI gramStr = (IRI) stmt.getObject();
				if (gramStr.equals(GSVocab.TERM_GRAMMAR_AS)) {
					term.setTermGrammar(TermGrammar.AS);
				} else if (gramStr.equals(GSVocab.TERM_GRAMMAR_AV)) {
					term.setTermGrammar(TermGrammar.AV);
				} else if (gramStr.equals(GSVocab.TERM_GRAMMAR_PP)) {
					term.setTermGrammar(TermGrammar.PP);
				} else if (gramStr.equals(GSVocab.TERM_GRAMMAR_SA)) {
					term.setTermGrammar(TermGrammar.SA);
				} else if (gramStr.equals(GSVocab.TERM_GRAMMAR_SV)) {
					term.setTermGrammar(TermGrammar.SV);
				} else if (gramStr.equals(GSVocab.TERM_GRAMMAR_V)) {
					term.setTermGrammar(TermGrammar.V);
				}
			}
		}
	}
	
	/**
	 * BF/BS and ZSP/QSP relations
	 * @param descriptors
	 * @param nonDescriptors
	 */
	private void handleLabelRelations(List<Term> descriptors, List<Term> nonDescriptors, Map<String, IRI> termIdLabelMap) {
		for (Term termD : descriptors) {
			//relations BF/BS between Descriptor and NonDescriptor
			for (Term termN : nonDescriptors) {
				//BF/BS and VV/ABK relations are only between terms with same language
				if (termD.getTermLanguage().equals(termN.getTermLanguage())) { 
					//distinguish from BF/BS relations and ABK/VV relations
					String askQuery = "ASK { ?prefLabel " + NTriplesUtil.toNTriplesString(GSVocab.ABBREVIATION) + " "
							+ NTriplesUtil.toNTriplesString(termIdLabelMap.get(termN.getTermId())) + "}";
					BooleanQuery bq = connection.prepareBooleanQuery(askQuery);
					if (bq.evaluate()) { //is abbreviation
						//ABK fullForm -> abbreviation; VV abbreviation -> fullForm
						termD.addRelation(new Relation(RelationType.ABK, termN.getTermId(), null, null));
						termN.addRelation(new Relation(RelationType.VV, termD.getTermId(), null, null));
					} else {
						termD.addRelation(new Relation(RelationType.BF, termN.getTermId(), null, null));
						termN.addRelation(new Relation(RelationType.BS, termD.getTermId(), null, null));
					}
				}
			}
			//relations QSP/ZSP between Descriptors
			for (Term termD2 : descriptors) {
				if (!termD.getTermLanguage().equals(termD2.getTermLanguage())) {
					if (termD.getTermLanguage().equals("DE")) { //ZSP DE->FR/EN
						termD.addRelation(new Relation(RelationType.ZSP, termD2.getTermId(), null, null));
						termD2.addRelation(new Relation(RelationType.QSP, termD.getTermId(), null, null));
					} else {
						termD.addRelation(new Relation(RelationType.QSP, termD2.getTermId(), null, null));
						termD2.addRelation(new Relation(RelationType.ZSP, termD.getTermId(), null, null));
					}
				}
			}
		}
	}
	
	private GraphQueryResult describeResource(IRI resource) {
		GraphQuery gq = connection.prepareGraphQuery("DESCRIBE " + NTriplesUtil.toNTriplesString(resource));
		setDataset(gq);
		return gq.evaluate();
	}
	
	private List<Term> getDescriptors(IRI resource, ObjectType objectType, Map<String, IRI> langLabelMap) {
		List<Term> termDescriptors = new ArrayList<>();
		//get all prefLabels
		String query = "SELECT DISTINCT ?label ?lit WHERE { "
				+ NTriplesUtil.toNTriplesString(resource) + " " + NTriplesUtil.toNTriplesString(SKOSXL.PREF_LABEL) + " ?label . "
				+ "?label "+ NTriplesUtil.toNTriplesString(SKOSXL.LITERAL_FORM) + " ?lit . }";
		
		TupleQueryResult prefLabelResult = connection.prepareTupleQuery(query).evaluate();
		
		while (prefLabelResult.hasNext()) {
			BindingSet bs = prefLabelResult.next();
			IRI prefLabel = (IRI) bs.getValue("label");
			Literal literalForm = (Literal) bs.getValue("lit");
			String label = literalForm.getLabel();
			String lang = literalForm.getLanguage().get().toUpperCase();
			
			langLabelMap.put(lang, prefLabel);
			
			Term termD = new Term(getTermId(prefLabel), objectType, label);
			termD.setTermType(TermType.D);
			termD.setTermLanguage(lang);
			
			termDescriptors.add(termD);
		}
		
		Term descriptorPivot = getPivot(termDescriptors);
		resourceDescriptorIdMap.put(getTermId(resource), descriptorPivot.getTermId());
		
		return termDescriptors;
	}
	
	private void enrichDescriptors(List<Term> descriptors, Term pivotTerm, IRI resource, Map<String, IRI> langLabelMap) {
		for (Term term : descriptors) {
			IRI label = langLabelMap.get(term.getTermLanguage());
			IRI sourceRes; //resource from where to get the info use to enrich the attributes
			if (term.equals(pivotTerm)) {
				sourceRes = resource;
			} else {
				sourceRes = label;
			}
			// termModifiedDate, termCreatedDate, targetGroup, objectSubtype, appName, termHoursValue, cluster, subjectGroup/subjectGroupName
			enrichTermAttributes(term, sourceRes);
			// termStatus, termFacet, termQualifier, termDisplayStatus, termGrammar
			enrichTermFromLabel(term, label);
		}
	}
	
	private void enrichDescriptorNotes(List<Term> descriptors, Term pivotTerm, Map<String, IRI> langLabelMap) {
		//notes for the not pivot Descriptors, should be taken from from prefLabel instead of resource
		for (Term termD : descriptors) {
			if (!termD.equals(pivotTerm)) {
				IRI label = langLabelMap.get(termD.getTermLanguage());
				//definition
				String noteQuery = "SELECT ?note WHERE { " + NTriplesUtil.toNTriplesString(label) + " " 
						+ NTriplesUtil.toNTriplesString(SKOS.DEFINITION) + " ?note . }";
				TupleQueryResult res = connection.prepareTupleQuery(noteQuery).evaluate();
				if (res.hasNext()) {
					String note = res.next().getBinding("note").getValue().stringValue();
					TermNote termNote = new TermNote(note);
					termNote.setLabel(NoteLabel.Definition);
					termD.addTermNote(termNote);
				}
				//history
				noteQuery = "SELECT ?note WHERE { " + NTriplesUtil.toNTriplesString(label) + " " 
						+ NTriplesUtil.toNTriplesString(SKOS.HISTORY_NOTE) + " ?note . }";
				res = connection.prepareTupleQuery(noteQuery).evaluate();
				if (res.hasNext()) {
					String note = res.next().getBinding("note").getValue().stringValue();
					TermNote termNote = new TermNote(note);
					termNote.setLabel(NoteLabel.History);
					termD.addTermNote(termNote);
				}
			}
		}
	}
	
	private List<Term> getNonDescriptors(IRI resource, ObjectType objectType, Map<String, IRI> termIdLabelMap) {
		List<Term> termsNonDescriptor = new ArrayList<>();
		//Alt labels
		String query = "SELECT DISTINCT ?label ?lit WHERE { "
				+ NTriplesUtil.toNTriplesString(resource) + " " + NTriplesUtil.toNTriplesString(SKOSXL.ALT_LABEL) + " ?label . "
				+ "?label "+ NTriplesUtil.toNTriplesString(SKOSXL.LITERAL_FORM) + " ?lit . }";
		TupleQueryResult altLabelResult = connection.prepareTupleQuery(query).evaluate();
		while (altLabelResult.hasNext()) {
			BindingSet bs = altLabelResult.next();
			IRI altLabel = (IRI) bs.getValue("label");
			Literal literalForm = (Literal) bs.getValue("lit");
			String label = literalForm.getLabel();
			String lang = literalForm.getLanguage().get().toUpperCase();
			
			Term termN = new Term(getTermId(altLabel), objectType, label);
			termN.setTermType(TermType.N);
			termN.setTermLanguage(lang);

			termIdLabelMap.put(termN.getTermId(), altLabel);
			
			// termModifiedDate, termCreatedDate, targetGroup, objectSubtype, appName, termHoursValue, cluster, subjectGroup/subjectGroupName
			enrichTermAttributes(termN, altLabel);
			//termStatus, termFacet, termQualifier, termDisplayStatus, termGrammar
			enrichTermFromLabel(termN, altLabel);
			
			termsNonDescriptor.add(termN);
		}
		return termsNonDescriptor;
	}
	
	/**
	 * 
	 * @param resource the compound equivalence instance (:k_...)
	 * @param objectType
	 * @param langLabelMap
	 * @return
	 */
	private List<Term> getTermsK(IRI resource, ObjectType objectType, Map<String, IRI> langLabelMap) {
		List<Term> termsDescriptor = new ArrayList<>();
		//get all prefLabels
		String query = "SELECT DISTINCT ?label ?lit WHERE { "
				+ NTriplesUtil.toNTriplesString(resource) + " " + NTriplesUtil.toNTriplesString(SKOSThes.PLUS_UF) + " ?label . "
				+ "?label "+ NTriplesUtil.toNTriplesString(SKOSXL.LITERAL_FORM) + " ?lit . }";
		
		TupleQueryResult prefLabelResult = connection.prepareTupleQuery(query).evaluate();
		
		while (prefLabelResult.hasNext()) {
			BindingSet bs = prefLabelResult.next();
			IRI prefLabel = (IRI) bs.getValue("label");
			Literal literalForm = (Literal) bs.getValue("lit");
			String label = literalForm.getLabel();
			String lang = literalForm.getLanguage().get().toUpperCase();
			
			langLabelMap.put(lang, prefLabel);
			
			Term termK = new Term(getTermId(prefLabel), objectType, label);
			termK.setTermType(TermType.K);
			termK.setTermLanguage(lang);
			
			termsDescriptor.add(termK);
		}
		return termsDescriptor;
	}
	
	private Term getPivot(List<Term> descriptors) {
		Term pivotTerm = null;
		for (Term term : descriptors) {
			if (term.getTermLanguage().equals("DE")) {
				pivotTerm = term;
				break;
			} if (
				term.getTermLanguage().equals("EN") && (pivotTerm == null || pivotTerm.getTermLanguage().equals("FR")) ||
				term.getTermLanguage().equals("FR") && (pivotTerm == null)
			) {
				pivotTerm = term;
			}
		}
		return pivotTerm;
	}
	
	/**
	 * It is possible that some term D are retrieved multiple times from xLabel
	 * that are prefLabel of multiple concepts.
	 * (e.g. 025145 is a term D with lang EN and is ZSP of 020576 and 020659,
	 * so it means that in rdf it will be a prefLabel (translation) of two concepts)
	 * so it is necessary to avoid duplicates 
	 * @param terms
	 */
	private void addTermsWithoutDuplicate(Term term) {
		Term termInZthes = zThes.getTermById(term.getTermId());
		if (termInZthes == null) {
			zThes.addTerm(term);
		} else {
			//add the relations of term to the relation of termInZthes (avoiding duplicates here as well)
			for (Relation relOfTerm : term.getRelations()) {
				boolean alreadyInRelsOfTermInZthes = false;
				for (Relation relOfTermInZthes : termInZthes.getRelations()) {
					if (
						relOfTermInZthes.getRelationType() == relOfTerm.getRelationType() &&
						relOfTermInZthes.getTermId().equals(relOfTerm.getTermId())
					) {//same relation => relation of term is already in termInZthes
						alreadyInRelsOfTermInZthes = true;
						break;
					}
				}
				if (!alreadyInRelsOfTermInZthes) {
					termInZthes.addRelation(relOfTerm);
				}
			}
		}
	}
	
	private String getTermId(IRI resource) {
		String localName = resource.getLocalName();
		return localName.substring(localName.lastIndexOf("_")+1);
	}
	
	private void setDataset(Query query) {
		SimpleDataset dataset = new SimpleDataset();
		dataset.addDefaultGraph(baseURI);
		query.setDataset(dataset);
	}
	
	
	
}
