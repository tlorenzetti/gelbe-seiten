package it.uniroma2.art.gelbeseiten.mapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import it.uniroma2.art.gelbeseiten.model.AppName;
import it.uniroma2.art.gelbeseiten.model.ObjectSubtype;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Relation;
import it.uniroma2.art.gelbeseiten.model.RelationType;
import it.uniroma2.art.gelbeseiten.model.TargetGroup;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermEntity;
import it.uniroma2.art.gelbeseiten.model.TermFacet;
import it.uniroma2.art.gelbeseiten.model.TermGrammar;
import it.uniroma2.art.gelbeseiten.model.TermNote;
import it.uniroma2.art.gelbeseiten.model.TermNote.NoteLabel;
import it.uniroma2.art.gelbeseiten.model.TermStatus;
import it.uniroma2.art.gelbeseiten.model.TermType;
import it.uniroma2.art.gelbeseiten.model.Zthes;
import it.uniroma2.art.gelbeseiten.vocabulary.GSOnto;
import it.uniroma2.art.gelbeseiten.vocabulary.GSVocab;
import it.uniroma2.art.gelbeseiten.vocabulary.SKOSThes;
import it.uniroma2.art.gelbeseiten.vocabulary.VBStatus;

public class ZthesToRdfMapper {
	
	private Zthes zThes;
	private RepositoryConnection connection;
	private SimpleValueFactory vf;
	private IRI baseURI;
	
	private Multimap<RelationType, String> wrongTermTypeRelations = ArrayListMultimap.create(); //relationType - details relation
	private Multimap<RelationType, String> wrongObjectTypeRelations = ArrayListMultimap.create(); //relationType - details relation
	
	private Multimap<String, Term> subTopicToTopicsMap = ArrayListMultimap.create(); //subTopic id (topConcept) - topic (scheme) map
	
	public ZthesToRdfMapper(Zthes zThes) {
		this.zThes = zThes;

		Repository rep = new SailRepository(new MemoryStore());
		rep.initialize();
		connection = rep.getConnection();
		connection.setNamespace(GSOnto.NS.getPrefix(), GSOnto.NS.getName());
		
		vf = SimpleValueFactory.getInstance();
		baseURI = vf.createIRI(GSOnto.NAMESPACE);
	}
	
	public void map() {
		
		//First of all map all the Topics (Concept schemes)
		System.out.print("Mapping Topics... ");
		mapTopics(zThes.getTermsByObjectType(ObjectType.T)); //Thema - Topic
		System.out.println("Done");
		
		//Then all the Subtopic (Top concepts)
		System.out.print("Mapping Subtopics... ");
		mapSubtopics(zThes.getTermsByObjectType(ObjectType.R));//Rubrik - Subtopic
		System.out.println("Done");
		
		//Heading (concepts)
		System.out.print("Mapping Headings... ");
		mapHeadings(zThes.getTermsByObjectType(ObjectType.B));//Branche - Heading
		System.out.println("Done");
		
		//Brand
		System.out.print("Mapping Brands... ");
		mapBrands(zThes.getTermsByObjectType(ObjectType.M));//Marke - Brand
		System.out.println("Done");
		
		//Controlled keyword (concepts)
		System.out.print("Mapping Controlled keywords... ");
		mapKeywords(zThes.getTermsByObjectType(ObjectType.Q));//QualifiziertesKeyword - Controlled keyword
		System.out.println("Done");
		
		//Name
		System.out.print("Mapping Names... ");
		mapNames(zThes.getTermsByObjectType(ObjectType.NA));
		System.out.println("Done");
		
		//Specials
		System.out.print("Mapping Specials... ");
		mapSpecials(zThes.getTermsByObjectType(ObjectType.N));
		System.out.println("Done");
		
		//ingore ObjectType.Z as requested
		
		if (wrongTermTypeRelations.size() > 0) {
			System.out.println(wrongTermTypeRelations.size() + " relations skipped due to invalid term type:");
			for (RelationType relationType : wrongTermTypeRelations.keySet()) {
				Collection<String> wrongRels = wrongTermTypeRelations.get(relationType);
				System.out.println(wrongRels.size() + " for relation " + relationType + ":");
				int idx = 0;
				for (String rel : wrongRels) {
					System.out.println(rel);
					idx++;
					if (idx == 10) {
						System.out.println((wrongRels.size() - idx) +  " more...");
						break;
					}
				}
			}
		}
		
		if (wrongObjectTypeRelations.size() > 0) {
			System.out.println(wrongObjectTypeRelations.size() + " relations skipped due to invalid object type:");
			for (RelationType relationType : wrongObjectTypeRelations.keySet()) {
				Collection<String> wrongRels = wrongObjectTypeRelations.get(relationType);
				System.out.println(wrongRels.size() + " for relation " + relationType + ":");
				int idx = 0;
				for (String rel : wrongRels) {
					System.out.println(rel);
					idx++;
					if (idx == 10) {
						System.out.println((wrongRels.size() - idx) +  " more...");
						break;
					}
				}
			}
		}
	}
	
	/*
	 * The following mapping methods, for each term, if it is a Descriptor (term type D),
	 * create the resource foreseen by the transformation and map its attributes.
	 * In any case (Descriptor or Non-Descriptor) map also the relation.
	 * When the term involved in the relation is a Non-Descriptor, map its attributes 
	 * (since the mapAttributes() has been skipped initially for the Non-Descriptor)
	 */
	
	/**
	 * Maps to rdf a term which objectType element is T (Thema-Topic)
	 * @param term
	 */
	private void mapTopics(Collection<Term> terms) {
		for (Term term : terms) {
			IRI scheme = createConceptScheme(term); //there are no translations for topic, so don't care to get the pivot Term
			mapAttributes(scheme, getXLabelIRI(term), term);
			connection.add(scheme, RDF.TYPE, GSVocab.TOPIC, baseURI);
			for (Relation relation : term.getRelations()) {
				if (!isRelationCompliant(term, relation)) {
					logUnhanldedRelationForWrongTermType(term, relation);
					continue;
				}
				if (relation.getRelationType() == RelationType.UB) {
					/* UB: term(D)->relation.term(D), T->R
					 * <topic> rdf:type <ConceptScheme>
					 * <subTopic> skos:topConceptOf <topic>
					 * <subTopic> skos:inScheme <topic>
					 */
					if (relation.getObjectType() == ObjectType.R || relation.getObjectType() == ObjectType.N){ //handle also the special topic
						TermEntity pivotTermRelation = zThes.getPivotTerm(relation);
						IRI topConcept = createConcept(pivotTermRelation);
						connection.add(topConcept, SKOS.TOP_CONCEPT_OF, scheme, baseURI);
						connection.add(topConcept, SKOS.IN_SCHEME, scheme, baseURI);
						subTopicToTopicsMap.put(pivotTermRelation.getTermId(), term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else {
					logUnhandledRelation(term, relation);
				}
			}
		}
	}
	
	/**
	 * Maps to rdf a term which objectType element is R (Rubrik-Subtopic)
	 * @param term
	 */
	private void mapSubtopics(Collection<Term> terms) {
		for (Term term : terms) {
			
			//if term is a Descriptor, generate a concept 
			IRI topConcept = null;
			TermEntity pivotTerm = null;
			if (term.getTermType() == TermType.D) {
				pivotTerm = term; //there are no translations for topics, so the pivotTerm is the same term
				topConcept = createConcept(pivotTerm);
				mapAttributes(topConcept, getXLabelIRI(term), term, pivotTerm);
			}
			
//			IRI topConcept = createConcept(term);//there are no translations for topConcept, so don't care to get the pivot Term
//			mapAttributes(topConcept, getXLabelIRI(term), term);
			
			for (Relation relation : term.getRelations()) {
				if (!isRelationCompliant(term, relation)) {
					logUnhanldedRelationForWrongTermType(term, relation);
					continue;
				}
				if (relation.getRelationType() == RelationType.UB) {
					/* UB: term(D)->relation.term(D), R->B
					 * <heading> skos:broader <subTopic>
					 * <heading> skos:inScheme <topic>
					 */
					if (relation.getObjectType() == ObjectType.B){
						IRI concept = createConcept(zThes.getPivotTerm(relation));
						connection.add(concept, SKOS.BROADER, topConcept, baseURI);
						//add concept to the same scheme of topConcept.
						Collection<Term> topics = getTopicOfSubtopic(term);
						for (Term topic : topics) {
							IRI scheme = createConceptScheme(topic);
							connection.add(concept, SKOS.IN_SCHEME, scheme, baseURI);
						}
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else {
					logUnhandledRelation(term, relation);
				}
			}
		}
	}
	
	/**
	 * Maps to rdf a term which objectType element is B (Branche-Heading)
	 * @param term
	 */
	private void mapHeadings(Collection<Term> terms) {
		for (Term term : terms) {
			
			//if term is a Descriptor, generate a concept 
			IRI headingConcept = null;
			TermEntity pivotTerm = null;
			if (term.getTermType() == TermType.D) {
				pivotTerm = zThes.getPivotTerm(term);
				headingConcept = createConcept(pivotTerm);
				mapAttributes(headingConcept, getXLabelIRI(term), term, pivotTerm);
			}
			
			for (Relation relation : term.getRelations()) {
				if (!isRelationCompliant(term, relation)) {
					logUnhanldedRelationForWrongTermType(term, relation);
					continue;
				}
				
				if (relation.getObjectType() == ObjectType.Z) {
					continue; //ignored object type Z as requested
				}
				
				if (relation.getRelationType() == RelationType.BF) {
					//BF: term(D)->relation.term(N), B->B, so term describes the concept, relation.term describes the altLabel
					if (relation.getObjectType() == ObjectType.B) {
						IRI altLabelIRI = createXLabel(relation);
						connection.add(headingConcept, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BS) {
					//BS: term(N)->relation.term(D), B->B, so term describes the altLabel, relation.term describes the concept
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						IRI altLabelIRI = createXLabel(term);
						connection.add(conceptRel, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
						mapAttributes(altLabelIRI, altLabelIRI, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BFA) {
					if (relation.getTermType() == TermType.A) {
						/* 
						 * BFA: term(D)->relation.term(A), B->B
						 * <conceptD> gs:access <accessRelationship>
						 * <accessRelationship> rdf:type gs:AccessRelationship
						 * <accessRelationship> gs:entry  <objectA>
						 * <objectA> rdf:type gs:AccessEntry
						 * <accessRelationship> gs:weight <weight value>^^xsd:decimal
						 */
						if (relation.getObjectType() == ObjectType.B) {
							TermEntity pivotRelationTerm = zThes.getPivotTerm(relation);
							IRI accessRelationship = createAccessRelationship(term, relation);
							IRI objectA = createAccessEntry(pivotRelationTerm);
							connection.add(headingConcept, GSVocab.ACCESS, accessRelationship, baseURI);
							connection.add(accessRelationship, GSVocab.ENTRY, objectA, baseURI);
							if (relation.getWeight() != 0.0f) {
								Literal weight = vf.createLiteral(Float.toString(relation.getWeight()), XMLSchema.FLOAT);
								connection.add(accessRelationship, GSVocab.WEIGHT, weight);
							}
						} else {
							logUnhanldedRelationForWrongObjectType(term, relation);
						}
					} else if (relation.getTermType() == TermType.N) {
						/*
						 * BFA: term(D)->relation.term(N), B->B
						 * For term of type N, retrieve the concept for which N is an skosxl:altLabel 
						 * (the D term to which the N term is attached) and then:
						 * 
						 * <conceptD> gs:access <accessRelationship>
						 * <accessRelationship> rdf:type gs:AccessRelationship
						 * <accessRelationship> gs:entry  <found_concept_for_N>
						 * <found_concept_for_N> rdf:type gs:AccessEntry
						 * <accessRelationship> gs:weight <weight value>^^xsd:decimal
						 */
						if (relation.getObjectType() == ObjectType.B) {
							TermEntity pivotRelationTerm = zThes.getPivotTerm(getTermOfAltLabel(relation));
							IRI accessRelationship = createAccessRelationship(pivotTerm, pivotRelationTerm);
							IRI conceptForN = createConcept(pivotRelationTerm);
							connection.add(headingConcept, GSVocab.ACCESS, accessRelationship, baseURI);
							connection.add(accessRelationship, GSVocab.ENTRY, conceptForN, baseURI);
							connection.add(conceptForN, RDF.TYPE, GSVocab.ACCESS_ENTRY, baseURI);
							if (relation.getWeight() != 0.0f) {
								Literal weight = vf.createLiteral(Float.toString(relation.getWeight()), XMLSchema.FLOAT);
								connection.add(accessRelationship, GSVocab.WEIGHT, weight);
							}
						} else {
							logUnhanldedRelationForWrongObjectType(term, relation);
						}
					}
				} else if (relation.getRelationType() == RelationType.BSA) {
					/* 
					 * BSA: term(A)->relation.term(D), B->B
					 * <conceptD> gs:access <accessRelationship>
					 * <accessRelationship> rdf:type gs:AccessRelationship
					 * <accessRelationship> gs:entry  <objectA>
					 * <objectA> rdf:type gs:AccessEntry
					 * <accessRelationship> gs:weight <weight value>^^xsd:decimal
					 */
					if (term.getTermType() == TermType.A) {
						if (relation.getObjectType() == ObjectType.B) {
							TermEntity pivotTermA = zThes.getPivotTerm(term);
							TermEntity pivotRelationTerm = zThes.getPivotTerm(relation);
							IRI conceptTerm = createConcept(pivotRelationTerm);
							IRI accessRelationship = createAccessRelationship(pivotRelationTerm, pivotTermA);
							IRI accessEntry = createAccessEntry(pivotTermA);
							connection.add(conceptTerm, GSVocab.ACCESS, accessRelationship, baseURI);
							connection.add(accessRelationship, GSVocab.ENTRY, accessEntry, baseURI);
							if (relation.getWeight() != 0.0f) {
								Literal weight = vf.createLiteral(Float.toString(relation.getWeight()), XMLSchema.FLOAT);
								connection.add(accessRelationship, GSVocab.WEIGHT, weight);
							}
							mapAttributes(accessEntry, getXLabelIRI(term), term, pivotTermA);
						} else {
							logUnhanldedRelationForWrongObjectType(term, relation);
						}
					} else if (term.getTermType() == TermType.N) {
						/*
						 * BSA: term(N)->relation.term(D), B->B
						 * For term of type N, retrieve the concept for which N is an skosxl:altLabel 
						 * (the D term to which the N term is attached) and then:
						 * 
						 * <conceptD> gs:access <accessRelationship>
						 * <accessRelationship> rdf:type gs:AccessRelationship
						 * <accessRelationship> gs:entry  <found_concept_for_N>
						 * <found_concept_for_N> rdf:type gs:AccessEntry
						 * <accessRelationship> gs:weight <weight value>^^xsd:decimal
						 */
						if (relation.getObjectType() == ObjectType.B) {
							TermEntity pivotTermN = zThes.getPivotTerm(getTermOfAltLabel(term));
							TermEntity pivotRelationTerm = zThes.getPivotTerm(relation);
							IRI conceptRel = createConcept(pivotRelationTerm);
							IRI accessRelationship = createAccessRelationship(pivotRelationTerm, pivotTermN);
							IRI conceptForN = createConcept(pivotTermN);
							connection.add(conceptRel, GSVocab.ACCESS, accessRelationship, baseURI);
							connection.add(accessRelationship, GSVocab.ENTRY, conceptForN, baseURI);
							connection.add(conceptForN, RDF.TYPE, GSVocab.ACCESS_ENTRY, baseURI);
							if (relation.getWeight() != 0.0f) {
								Literal weight = vf.createLiteral(Float.toString(relation.getWeight()), XMLSchema.FLOAT);
								connection.add(accessRelationship, GSVocab.WEIGHT, weight);
							}
						} else {
							logUnhanldedRelationForWrongObjectType(term, relation);
						}
					}
				} else if (relation.getRelationType() == RelationType.TH) { 
					//TH: term(D)->relation.term(D), B->R. => term concept skos:broader relation.term concept
					if (relation.getObjectType() == ObjectType.R) {
						TermEntity pivotRelationTerm = zThes.getPivotTerm(relation);
						IRI broader = createConcept(pivotRelationTerm);
						connection.add(headingConcept, SKOS.BROADER, broader, baseURI);
						//add concept to the same scheme of broader.
						Collection<Term> topics = getTopicOfSubtopic(pivotRelationTerm);
						for (Term topic : topics) {
							IRI scheme = createConceptScheme(topic);
							connection.add(headingConcept, SKOS.IN_SCHEME, scheme, baseURI);
						}
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.QSP) {
					//QSP: term(D)->relation.term(D), B->B, translation from EN/FR to DE
					if (relation.getObjectType() == ObjectType.B) {
						IRI concept = createConcept(relation);
						IRI xLabel = createXLabel(term);
						connection.add(concept, SKOSXL.PREF_LABEL, xLabel, baseURI);
						mapAttributes(xLabel, xLabel, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.ZSP) {
					//ZSP: term(D)->relation.term(D), B->B, translation from DE to EN/FR
					if (term.getTermType() == TermType.D) {
						if (relation.getObjectType() == ObjectType.B) {
							IRI xLabel = createXLabel(relation);
							connection.add(headingConcept, SKOSXL.PREF_LABEL, xLabel, baseURI);
						} else {
							logUnhanldedRelationForWrongObjectType(term, relation);
						}
					}//found cases where term type is A, ignore it since it is an error
				} else if (relation.getRelationType() == RelationType.VB) {
					//VB: term(D)->relation.term(D), B->B, term skos:related relation.term
					if (relation.getObjectType() == ObjectType.B) {
						IRI related = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, SKOS.RELATED, related, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.MRK) {
					/* MRK: term(D)->relation.term(D), B->M,
					 * <concept_for_brand> skos:inScheme gs:Brands
					 * <concept_for_brand> skos:topConcept of gs:Brands
					 * gs:Brands rdf:type skos:ConceptScheme
					 * <concept_for_product> gs:brand <concept for brand>
					 */
					if (relation.getObjectType() == ObjectType.M) {
						IRI brand = createConcept(zThes.getPivotTerm(relation));
						connection.add(brand, SKOS.IN_SCHEME, GSVocab.BRANDS, baseURI);
						connection.add(brand, SKOS.TOP_CONCEPT_OF, GSVocab.BRANDS, baseURI);
						connection.add(headingConcept, GSVocab.BRAND_PROP, brand, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.ID) {
					//ID: term(D)->relation.term(D), B->B are mapped as skos:exactMatch
					if (relation.getObjectType() == ObjectType.B) {
						IRI targetConcept = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, SKOS.EXACT_MATCH, targetConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.OBB) {
					/* OBB: term(D)->relation.term(D), B->B
					 * <concept for term1> gs:broaderRestricted <concept for term2>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, GSVocab.BROADER_RESTRICTED, conceptRel, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.UBB) {
					/* UBB: term(D)->relation.term(D), B->B
					 * <concept for term2> gs:broaderRestricted <concept for term1>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(conceptRel, GSVocab.BROADER_RESTRICTED, headingConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.OBA) {
					/* OBA: term(D)->relation.term(D), B->B
					 * <concept for term1> skos-thes:broaderGeneric <concept for term2> 
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, SKOSThes.BROADER_GENERIC, conceptRel, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.UBA) {
					/* UBA: term(D)->relation.term(D), B->B
					 * <concept for term2> skos-thes:broaderGeneric <concept for term1>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(conceptRel, SKOSThes.BROADER_GENERIC, headingConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.OBP) {
					/* OBP: term(D)->relation.term(D), B->B
					 * <concept for term1> skos-thes:broaderPartitive <concept for term2>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, SKOSThes.BROADER_PARTITIVE, conceptRel, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.UBP) {
					/* UBP: term(D)->relation.term(D), B->B
					 * <concept for term2> skos-thes:broaderPartitive <concept for term1>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(conceptRel, SKOSThes.BROADER_PARTITIVE, headingConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.OBZ) {
					/* OBZ: term(D)->relation.term(D), B->B
					 * <concept for term1> gs:broaderOther <concept for term2>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, GSVocab.BROADER_OTHER, conceptRel, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.UBZ) {
					/* UBZ: term(D)->relation.term(D), B->B
					 * <concept for term2> gs:broaderOther <concept for term1>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(conceptRel, GSVocab.BROADER_OTHER, headingConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.NB) {
					/* NB: term(D)->relation.term(D)
					 * <concept for term1> gs:differentFrom <concept for term 2>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI targetConcept = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, GSVocab.DIFFERENT_FROM, targetConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.QS) {
					//QS: term(D)->relation.term(D), B->B are mapped as skos:closeMatch
					if (relation.getObjectType() == ObjectType.B) {
						IRI targetConcept = createConcept(zThes.getPivotTerm(relation));
						connection.add(headingConcept, SKOS.CLOSE_MATCH, targetConcept, baseURI);
						connection.add(targetConcept, SKOS.CLOSE_MATCH, headingConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.GG) {
					/* GG: term(D)->relation.term(D), B->B
					 * <xLabel for term1> gs:antonym <xLabel for term2> 
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI xLabelTerm = createXLabel(term);
						IRI xLabelRel = createXLabel(relation);
						connection.add(xLabelTerm, GSVocab.ANTONYM, xLabelRel, baseURI);
						connection.add(xLabelRel, GSVocab.ANTONYM, xLabelTerm, baseURI);
						mapAttributes(xLabelTerm, xLabelTerm, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.KB) {
					/* KB: term(D)->relation.term(K), B->B
					 * create an xLabel for K: xl_<termK_ID>
					 * create an instance for K: k_<termK_ID>
					 * <instance K>  rdf:type iso-thes:CompoundEquivalence
					 * <instance K>  iso-thes:plusUF  <xlabel for K>
					 * for each term D connected to the K:
	  				 * <instance K>  iso-thes:plusUse <xLabel of the various D terms> 
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI xLabelD = createXLabel(term);
						IRI xLabelK = createXLabel(relation);
						IRI instanceK = vf.createIRI(GSOnto.NAMESPACE, "k_" + relation.getTermId());
						connection.add(instanceK, RDF.TYPE, SKOSThes.COMPOUND_EQUIVALENCE, baseURI);
						connection.add(instanceK, SKOSThes.PLUS_UF, xLabelK, baseURI);
						connection.add(instanceK, SKOSThes.PLUS_USE, xLabelD, baseURI);
						mapAttributes(xLabelD, xLabelD, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BK) {
					//BK: term(K)->relation.term(D), B->B/M see KB
					if (relation.getObjectType() == ObjectType.B || relation.getObjectType() == ObjectType.M) {
						IRI xLabelD = createXLabel(relation);
						IRI xLabelK = createXLabel(term);
						IRI instanceK = vf.createIRI(GSOnto.NAMESPACE, "k_" + term.getTermId());
						connection.add(instanceK, RDF.TYPE, SKOSThes.COMPOUND_EQUIVALENCE, baseURI);
						connection.add(instanceK, SKOSThes.PLUS_UF, xLabelK, baseURI);
						connection.add(instanceK, SKOSThes.PLUS_USE, xLabelD, baseURI);
						mapAttributes(instanceK, xLabelK, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BQK) {
					/* 
					 * BQK: term(D)->relation.term(D), B->Q
					 * <concept for heading> gs:controlledKeyword <controlledKeywordRelationship>
					 * <controlledKeywordRelationship>  rdf:type gs:ControlledKeywordRelationship
					 * <controlledKeywordRelationship>  gs:keyword <concept for controlled keyword>
					 * <controlledKeywordRelationship>  gs:weight <weight value>^^xsd:decimal
					 * <concept for controlled keyword> skos:inScheme gs:ControlledKeywords
					 * <concept for controlled keyword> skos:topConceptOf gs:ControlledKeywords
					 */
					if (relation.getObjectType() == ObjectType.Q) {
						TermEntity pivotRelTerm = zThes.getPivotTerm(relation);
						IRI keywordConcept = createConcept(pivotRelTerm);
						IRI controlledKeywordRelation = createControlledKeywordRelationship(pivotTerm, pivotRelTerm);
						connection.add(headingConcept, GSVocab.CONTROLLED_KEYWORD, controlledKeywordRelation, baseURI);
						connection.add(controlledKeywordRelation, GSVocab.KEYWORD, keywordConcept, baseURI);
						if (relation.getWeight() != 0.0f) {
							Literal weight = vf.createLiteral(Float.toString(relation.getWeight()), XMLSchema.FLOAT);
							connection.add(controlledKeywordRelation, GSVocab.WEIGHT, weight);
						}
						connection.add(keywordConcept, SKOS.IN_SCHEME, GSVocab.CONTROLLED_KEYWORDS, baseURI);
						connection.add(keywordConcept, SKOS.TOP_CONCEPT_OF, GSVocab.CONTROLLED_KEYWORDS, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.ABK) {
					/* ABK: term(D)->relation.term(N), B->B
					 * <concept of the full form (D)> skosxl:altLabel <xLabel of the abbreviated form (N)>
					 * <xLabel of the full form (D)> gs:abbreviation <xLabel of the abbreviated form (N)>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI xLabelTerm = createXLabel(term);
						IRI xLabelRel = createXLabel(relation);
						connection.add(headingConcept, SKOSXL.ALT_LABEL, xLabelRel, baseURI);
						connection.add(xLabelTerm, GSVocab.ABBREVIATION, xLabelRel, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.VV) {
					/* VV: term(N)->relation.term(D), B->B
					 * <concept of the full form (D)> skosxl:altLabel <xLabel of the abbreviated form (N)>
					 * <xLabel of the full form (D)> gs:abbreviation <xLabel of the abbreviated form (N)> 
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI concept = createConcept(zThes.getPivotTerm(relation));
						IRI xLabelTerm = createXLabel(term);
						IRI xLabelRel = createXLabel(relation);
						connection.add(concept, SKOSXL.ALT_LABEL, xLabelTerm, baseURI);
						connection.add(xLabelRel, GSVocab.ABBREVIATION, xLabelTerm, baseURI);
						mapAttributes(xLabelTerm, xLabelTerm, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BZT || relation.getRelationType() == RelationType.ZTB) {
					//don't do nothing, ignore these relations
				} else {
					logUnhandledRelation(term, relation);
				}
			}
		}
	}
	
	
	/**
	 * Maps to rdf a term which objectType element is M (Marke-Brand)
	 * @param term
	 */
	private void mapBrands(Collection<Term> terms) {
		for (Term term : terms) {
				
			//if term is a Descriptor, generate a concept and put it in gs:Brands scheme. 
			IRI brandConcept = null;
			TermEntity pivotTerm = zThes.getPivotTerm(term);
			if (term.getTermType() == TermType.D) {
				brandConcept = createConcept(pivotTerm);
				connection.add(brandConcept, SKOS.IN_SCHEME, GSVocab.BRANDS, baseURI); 
				connection.add(brandConcept, SKOS.TOP_CONCEPT_OF, GSVocab.BRANDS, baseURI);
				connection.add(brandConcept, RDF.TYPE, GSVocab.BRAND_CLASS, baseURI);
				mapAttributes(brandConcept, getXLabelIRI(term), term, pivotTerm);
			}
			
			for (Relation relation : term.getRelations()) {
				if (!isRelationCompliant(term, relation)) {
					logUnhanldedRelationForWrongTermType(term, relation);
					continue;
				}
				
				if (relation.getRelationType() == RelationType.PRM) {
					/* PRM: term(D)->relation.term(D), M->B,
					 * <concept_for_product> gs:brand <concept for brand>
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI product = createConcept(zThes.getPivotTerm(relation));
						connection.add(product, GSVocab.BRAND_PROP, brandConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BS) {
					//BS: term(N)->relation.term(D), M->M
					if (relation.getObjectType() == ObjectType.M) {
						IRI brandRel = createConcept(zThes.getPivotTerm(relation));
						IRI altLabelIRI = createXLabel(term);
						connection.add(brandRel, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
						connection.add(brandRel, SKOS.IN_SCHEME, GSVocab.BRANDS, baseURI);
						mapAttributes(altLabelIRI, altLabelIRI, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BF) {
					//BF: term(D)->relation.term(N), M->M
					if (relation.getObjectType() == ObjectType.M) {
						IRI altLabelIRI = createXLabel(relation);
						connection.add(brandConcept, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.VB) {
					//VB: term(D)->relation.term(D), M->M, term skos:related relation.term
					if (relation.getObjectType() == ObjectType.M) {
						IRI related = createConcept(zThes.getPivotTerm(relation));
						connection.add(brandConcept, SKOS.RELATED, related, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.KB) {
					/* KB: term(D)->relation.term(K), M->B
					 * create an xLabel for K: xl_<termK_ID>
					 * create an instance for K: k_<termK_ID>
					 * <instance K>  rdf:type iso-thes:CompoundEquivalence
					 * <instance K>  iso-thes:plusUF  <xlabel for K>
					 * for each term D connected to the K:
	  				 * <instance K>  iso-thes:plusUse <xLabel of the various D terms> 
					 */
					if (relation.getObjectType() == ObjectType.B) {
						IRI xLabelBrand = getXLabelIRI(term);
						IRI xLabelK = createXLabel(relation);
						IRI instanceK = vf.createIRI(GSOnto.NAMESPACE, "k_" + relation.getTermId());
						connection.add(instanceK, RDF.TYPE, SKOSThes.COMPOUND_EQUIVALENCE, baseURI);
						connection.add(instanceK, SKOSThes.PLUS_UF, xLabelK, baseURI);
						connection.add(instanceK, SKOSThes.PLUS_USE, xLabelBrand, baseURI);
						//don't map attribute to term since it generate only a xLabel
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.OBA) {
					/* OBA: term(D)->relation.term(D), M->M
					 * <concept for term1> skos-thes:broaderGeneric <concept for term2> 
					 */
					if (relation.getObjectType() == ObjectType.M) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(conceptRel, SKOS.IN_SCHEME, GSVocab.BRANDS, baseURI);
						connection.add(brandConcept, SKOSThes.BROADER_GENERIC, conceptRel, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.UBA) {
					/* UBA: term(D)->relation.term(D), M->M
					 * <concept for term2> skos-thes:broaderGeneric <concept for term1>
					 */
					if (relation.getObjectType() == ObjectType.M) {
						IRI conceptRel = createConcept(zThes.getPivotTerm(relation));
						connection.add(conceptRel, SKOS.IN_SCHEME, GSVocab.BRANDS, baseURI);
						connection.add(conceptRel, SKOSThes.BROADER_GENERIC, brandConcept, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else {
					logUnhandledRelation(term, relation);
				}
			}
		}
	}
	
	/**
	 * Maps to rdf a term which objectType element is Q (QualifiziertesKeyword-Controlled keyword)
	 * @param term
	 */
	private void mapKeywords(Collection<Term> terms) {
		for (Term term : terms) {
			
			//if term is a Descriptor, generate a concept and put it in gs:ControlledKeywords scheme. 
			IRI keywordConcept = null;
			if (term.getTermType() == TermType.D) {
				TermEntity pivotTerm = zThes.getPivotTerm(term);
				keywordConcept = createConcept(pivotTerm);
				connection.add(keywordConcept, SKOS.IN_SCHEME, GSVocab.CONTROLLED_KEYWORDS, baseURI);
				mapAttributes(keywordConcept, getXLabelIRI(term), term, pivotTerm);
			}
			
			for (Relation relation : term.getRelations()) {
				if (!isRelationCompliant(term, relation)) {
					logUnhanldedRelationForWrongTermType(term, relation);
					continue;
				}
				
				if (relation.getRelationType() == RelationType.BF) {
					//BF: term(D)->relation.term(N), Q->Q, so term describes the concept, relation.term describes the altLabel
					if (relation.getObjectType() == ObjectType.Q) {
						IRI altLabelIRI = createXLabel(relation);
						connection.add(keywordConcept, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BS) {
					//BS: term(N)->relation.term(D), Q->Q, so term describes the altLabel, relation.term describes the concept
					if (relation.getObjectType() == ObjectType.Q) {
						IRI concept = createConcept(zThes.getPivotTerm(relation));
						IRI altLabelIRI = createXLabel(term);
						connection.add(concept, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
						connection.add(concept, SKOS.IN_SCHEME, GSVocab.CONTROLLED_KEYWORDS, baseURI);
						mapAttributes(altLabelIRI, altLabelIRI, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.QKB) {
					/* 
					 * QKB: term(D)->relation.term(D), Q->B
					 * <concept for heading> gs:controlledKeyword <controlledKeywordRelationship>
					 * <controlledKeywordRelationship>  rdf:type gs:ControlledKeywordRelationship
					 * <controlledKeywordRelationship>  gs:keyword <concept for controlled keyword>
					 * <controlledKeywordRelationship>  gs:weight <weight value>^^xsd:decimal
					 * <concept for controlled keyword> skos:inScheme gs:ControlledKeywords
					 * <concept for controlled keyword> skos:topConceptOf gs:ControlledKeywords
					 */
					if (relation.getObjectType() == ObjectType.B) {
						TermEntity pivotTerm = zThes.getPivotTerm(term);
						TermEntity pivotRelTerm = zThes.getPivotTerm(relation);
						IRI conceptHeading = createConcept(pivotRelTerm);
						IRI controlledKeywordRelation = createControlledKeywordRelationship(pivotRelTerm, pivotTerm);
						connection.add(conceptHeading, GSVocab.CONTROLLED_KEYWORD, controlledKeywordRelation, baseURI);
						connection.add(controlledKeywordRelation, GSVocab.KEYWORD, keywordConcept, baseURI);
						if (relation.getWeight() != 0.0f) {
							Literal weight = vf.createLiteral(Float.toString(relation.getWeight()), XMLSchema.FLOAT);
							connection.add(controlledKeywordRelation, GSVocab.WEIGHT, weight);
						}
						connection.add(keywordConcept, SKOS.TOP_CONCEPT_OF, GSVocab.CONTROLLED_KEYWORDS, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.VB) {
					//VB: term(D)->relation.term(D), Q->Q, term skos:related relation.term
					if (relation.getObjectType() == ObjectType.Q) {
						IRI related = createConcept(zThes.getPivotTerm(relation));
						connection.add(keywordConcept, SKOS.RELATED, related, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else {
					logUnhandledRelation(term, relation);
				}
			}
		}
	}
	
	/**
	 * @param term
	 */
	private void mapSpecials(Collection<Term> terms) {
		for (Term term : terms) {
			/* 
			 * There are only two term of type N:
			 * (1)052666 that is considered as a normal topic 
			 * and (1)053516 considered as subtopic
			 */
			if (term.getTermId().equals("052666")) { //Considered as Topic
				mapTopics(Arrays.asList(term));
			} else if (term.getTermId().equals("053516")) { //Considered as Subtopic
				mapSubtopics(Arrays.asList(term));
			}
		}
	}
	
	private void mapNames(Collection<Term> terms) {
		connection.add(GSOnto.NAMES, RDF.TYPE, SKOS.CONCEPT_SCHEME, baseURI);
		
		for (Term term : terms) {
			
			//if term is a Descriptor, generate a concept and put in gs:Names scheme as topConcept. 
			IRI nameConcept = null;
			if (term.getTermType() == TermType.D) {
				TermEntity pivotTerm = zThes.getPivotTerm(term);
				nameConcept = createConcept(pivotTerm);
				connection.add(nameConcept, SKOS.IN_SCHEME, GSOnto.NAMES, baseURI);
				connection.add(nameConcept, SKOS.TOP_CONCEPT_OF, GSOnto.NAMES, baseURI);
				mapAttributes(nameConcept, getXLabelIRI(term), term, pivotTerm);
			}
			
			for (Relation relation : term.getRelations()) {
				if (!isRelationCompliant(term, relation)) {
					logUnhanldedRelationForWrongTermType(term, relation);
					continue;
				}
				
				if (relation.getRelationType() == RelationType.BF) {
					//BF: term(D)->relation.term(N), B->B, so term describes the concept, relation.term describes the altLabel
					if (relation.getObjectType() == ObjectType.NA) {
						IRI altLabelIRI = createXLabel(relation);
						connection.add(nameConcept, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.BS) {
					//BS: term(N)->relation.term(D), B->B, so term describes the altLabel, relation.term describes the concept
					if (relation.getObjectType() == ObjectType.NA) {
						IRI nameConceptRel = createConcept(zThes.getPivotTerm(relation));
						IRI altLabelIRI = createXLabel(term);
						connection.add(nameConceptRel, SKOS.TOP_CONCEPT_OF, GSOnto.NAMES, baseURI);
						connection.add(nameConceptRel, SKOS.IN_SCHEME, GSOnto.NAMES, baseURI);
						connection.add(nameConceptRel, SKOSXL.ALT_LABEL, altLabelIRI, baseURI);
						mapAttributes(altLabelIRI, altLabelIRI, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.ABK) {
					/* ABK: term(D)->relation.term(N), NA->NA
					 * <concept of the full form (D)> skosxl:altLabel <xLabel of the abbreviated form (N)>
					 * <xLabel of the full form (D)> gs:abbreviation <xLabel of the abbreviated form (N)>
					 */
					if (relation.getObjectType() == ObjectType.NA) {
						IRI xLabelTerm = getXLabelIRI(term);
						IRI xLabelRel = createXLabel(relation);
						connection.add(nameConcept, SKOSXL.ALT_LABEL, xLabelRel, baseURI);
						connection.add(xLabelTerm, GSVocab.ABBREVIATION, xLabelRel, baseURI);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else if (relation.getRelationType() == RelationType.VV) {
					/* VV: term(N)->relation.term(D), NA->NA
					 * <concept of the full form (D)> skosxl:altLabel <xLabel of the abbreviated form (N)>
					 * <xLabel of the full form (D)> gs:abbreviation <xLabel of the abbreviated form (N)> 
					 */
					if (relation.getObjectType() == ObjectType.NA) {
						IRI nameConceptRel = createConcept(zThes.getPivotTerm(relation));
						IRI xLabelTerm = createXLabel(term);
						IRI xLabelRel = createXLabel(relation);
						connection.add(nameConceptRel, SKOS.TOP_CONCEPT_OF, GSOnto.NAMES, baseURI);
						connection.add(nameConceptRel, SKOS.IN_SCHEME, GSOnto.NAMES, baseURI);
						connection.add(nameConceptRel, SKOSXL.ALT_LABEL, xLabelTerm, baseURI);
						connection.add(xLabelRel, GSVocab.ABBREVIATION, xLabelTerm, baseURI);
						mapAttributes(xLabelTerm, xLabelTerm, term);
					} else {
						logUnhanldedRelationForWrongObjectType(term, relation);
					}
				} else {
					logUnhandledRelation(term, relation);
				}
			}
		}
	}
	
	/**
	 * @param termResource the subject resource to enrich generated by the Term (could be a skosxl:Label too if the Term
	 * generated an xLabel (e.g. non-descriptor terms))
	 * @param termXLabel the xLabel of the resource. It is used to enrich some attribute proper of the xlabel,
	 * like termQualifier, termGrammar, termStatus,...
	 * @param term the term from where take the attributes value
	 */
	private void mapAttributes(IRI termResource, IRI termXLabel, Term term) {
		boolean isResourceXLabel = (termResource.stringValue().equals(termXLabel.stringValue()));
		
		//attributes only for labels
		enrichTermQualifier(termXLabel, term);
		enrichTermFacet(termXLabel, term);
		enrichTermDisplayStatus(termXLabel, term);
		enrichTermGrammar(termXLabel, term);
		enrichTermStatus(termXLabel, term);
		
		//attributes of all the resources
		enrichTermHoursValue(termResource, term);
		enrichTermCreatedDate(termResource, term);
		enrichTermModifiedDate(termResource, term);
		enrichNotes(termResource, term);
		
		//these attributes should be mapped only for resource NOT xLabel
		if (!isResourceXLabel) {
			//enrich appName only for topic, not Rubrik (it's redundant) and special 1052666 considered topic
			if (term.getObjectType() == ObjectType.T || term.getTermId().equals("1052666")) {
				enrichAppName(termResource, term);
			}
			enrichSubjectGroupName(termResource, term);
			enrichCluster(termResource, term);
			enrichTargetGroup(termResource, term);
		}
		
		//objectSubtype attribute can be attached both to concept (for term D) and xLabel (altLabel for term N)
		enrichObjectSubtype(termResource, term, isResourceXLabel);
	}
	
	/**
	 * @param termResource the subject resource to enrich generated by the Term (could be a skosxl:Label too if the Term
	 * generated an xLabel (e.g. non-descriptor terms))
	 * @param termXLabel the xLabel of the resource. It is used to enrich some attribute proper of the xlabel,
	 * like termQualifier, termGrammar, termStatus,...
	 * @param term the term from where take the attributes value
	 * @param pivotTerm used to check if term is a pivot term. In case it is not, the attributes are attacched to the 
	 * termXLabel
	 */
	private void mapAttributes(IRI termResource, IRI termXLabel, Term term, TermEntity pivotTerm) {
		if (!pivotTerm.getTermId().equals(term.getTermId())) {
			/*
			 * If term is not a pivot term, it didn't generate a concept/scheme/collection,
			 * it generated an xLabel instead. So, all its attributes should be attached to the xLabel rather
			 * than the resource
			 */
			termResource = termXLabel;
		}
		mapAttributes(termResource, termXLabel, term);
	}
	
	
	private void enrichObjectSubtype(IRI resource, Term term, boolean isResourceXLabel) {
		/*
		 * <resource> gs:headingType <headingType found>
		 * and if resource is not an xlabel
		 * <resource> skos:inScheme <scheme related to heading type>
		 */
		ObjectSubtype objSubtype = term.getObjectSubtype();
		if (objSubtype != null) {
			IRI headingType = null;
			IRI headingScheme = null;
			if (objSubtype == ObjectSubtype.BG) {
				headingType = GSVocab.HEADING_TYPE_BG;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.BQ) {
				headingType = GSVocab.HEADING_TYPE_BQ;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.DG) {
				headingType = GSVocab.HEADING_TYPE_DG;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.DW) {
				headingType = GSVocab.HEADING_TYPE_DW;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.DX) {
				headingType = GSVocab.HEADING_TYPE_DX;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.G) {
				headingType = GSVocab.HEADING_TYPE_G;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.K) {
				headingType = GSVocab.HEADING_TYPE_K;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.Q) {
				headingType = GSVocab.HEADING_TYPE_Q;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.V) {
				headingType = GSVocab.HEADING_TYPE_V;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.VG) {
				headingType = GSVocab.HEADING_TYPE_VG;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.VQ) {
				headingType = GSVocab.HEADING_TYPE_VQ;
				headingScheme = GSVocab.GRUNDNOMENKLATUR;
			} else if (objSubtype == ObjectSubtype.W) {
				headingType = GSVocab.HEADING_TYPE_W;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.X) {
				headingType = GSVocab.HEADING_TYPE_X;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.XX) {
				headingType = GSVocab.HEADING_TYPE_XX;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.Z) {
				headingType = GSVocab.HEADING_TYPE_Z;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.Z1) {
				headingType = GSVocab.HEADING_TYPE_Z1;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			} else if (objSubtype == ObjectSubtype.ZB) {
				headingType = GSVocab.HEADING_TYPE_ZB;
				headingScheme = GSVocab.WUNSCHBRANCHEN_UND_ZUGANGSBEGRIFFE;
			}
			if (headingType != null && headingScheme != null) {
				connection.add(resource, GSVocab.HEADING_TYPE, headingType, baseURI);
				if (!isResourceXLabel) {
					connection.add(resource, SKOS.IN_SCHEME, headingScheme, baseURI);
				}
			}
		}
	}
	
	/*
	 * Only for topic (T) and subtopic (R)
	 */
	private void enrichAppName(IRI resource, TermEntity entity) {
		AppName appName = entity.getAppName();
		IRI app = null;
		if (appName == AppName.GS_IPHONE) {
			app = GSVocab.GS_IPHONE;
		} else if (appName == AppName.GS_NAHE) {
			app = GSVocab.GS_NAHE;
		} else if (appName == AppName.GS_ONLINE) {
			app = GSVocab.GS_ONLINE;
		}
		if (app != null) {
			connection.add(resource, GSVocab.APP_PROP, app, baseURI);
		}
	}
	
	private void enrichTermQualifier(IRI xLabel, TermEntity entity) {
		String termQualifier = entity.getTermQualifier(); //"0" or "1"
		if (termQualifier != null) {
			Literal tqLiteral = vf.createLiteral(termQualifier, XMLSchema.INTEGER);
			connection.add(xLabel, GSVocab.QUALIFIER, tqLiteral, baseURI);
		}
	}
	
	private void enrichTermFacet(IRI xLabel, Term term) {
		TermFacet termFacet = term.getTermFacet();
		if (termFacet == TermFacet.B) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_B, baseURI);
		} else if (termFacet == TermFacet.E) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_E, baseURI);
		} else if (termFacet == TermFacet.F) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_F, baseURI);
		} else if (termFacet == TermFacet.G) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_G, baseURI);
		} else if (termFacet == TermFacet.I) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_I, baseURI);
		} else if (termFacet == TermFacet.N) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_N, baseURI);
		} else if (termFacet == TermFacet.P) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_P, baseURI);
		} else if (termFacet == TermFacet.T) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_T, baseURI);
		} else if (termFacet == TermFacet.V) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_V, baseURI);
		} else if (termFacet == TermFacet.X) {
			connection.add(xLabel, GSVocab.FACET, GSVocab.FACET_X, baseURI);
		}
	}
	
	private void enrichTermGrammar(IRI xLabel, Term term) {
		TermGrammar termGrammar = term.getTermGrammar();
		if (termGrammar == TermGrammar.AS) {
			connection.add(xLabel, GSVocab.GRAMMATICAL_STRUCTURE, GSVocab.TERM_GRAMMAR_AS, baseURI);
		} else if (termGrammar == TermGrammar.AV) {
			connection.add(xLabel, GSVocab.GRAMMATICAL_STRUCTURE, GSVocab.TERM_GRAMMAR_SV, baseURI);
		} else if (termGrammar == TermGrammar.PP) {
			connection.add(xLabel, GSVocab.GRAMMATICAL_STRUCTURE, GSVocab.TERM_GRAMMAR_PP, baseURI);
		} else if (termGrammar == TermGrammar.SA) {
			connection.add(xLabel, GSVocab.GRAMMATICAL_STRUCTURE, GSVocab.TERM_GRAMMAR_SA, baseURI);
		} else if (termGrammar == TermGrammar.SV) {
			connection.add(xLabel, GSVocab.GRAMMATICAL_STRUCTURE, GSVocab.TERM_GRAMMAR_SV, baseURI);
		} else if (termGrammar == TermGrammar.V) {
			connection.add(xLabel, GSVocab.GRAMMATICAL_STRUCTURE, GSVocab.TERM_GRAMMAR_V, baseURI);
		} 
	}
	
	private void enrichTermDisplayStatus(IRI xLabel, Term term) {
		String displayStatus = term.getTermDisplayStatus();
		if (displayStatus != null && displayStatus.equals("1")) {
			Literal statusBool = vf.createLiteral("true", XMLSchema.BOOLEAN);
			connection.add(xLabel, GSVocab.DISPLAY_STATUS, statusBool, baseURI); 
		}
	}
	
	private void enrichTermStatus(IRI xLabel, Term term) {
		TermStatus termStatus = term.getTermStatus();
		if (termStatus != null) {
			Literal status = vf.createLiteral(termStatus.toString(), XMLSchema.STRING);
			connection.add(xLabel, VBStatus.TERM_STATUS, status, baseURI);
		}
	}
	
	private void enrichSubjectGroupName(IRI concept, Term term) {
		String subjectGroupName = term.getSubjectGroupName();
		String subjectGroupCode = term.getSubjectGroup();
		if (subjectGroupName != null && subjectGroupCode != null) {
			String subjGrpLocalNameSuffix = "subjGrp_" + subjectGroupCode;
			
			IRI subjGrIri = vf.createIRI(GSOnto.NAMESPACE, subjGrpLocalNameSuffix);
			connection.add(concept, SKOS.IN_SCHEME, subjGrIri, baseURI);
			connection.add(subjGrIri, RDF.TYPE, SKOS.CONCEPT_SCHEME, baseURI);
			connection.add(subjGrIri, RDF.TYPE, GSVocab.SUBJECT_GROUP, baseURI);
			
			IRI xLabelIri = vf.createIRI(GSOnto.NAMESPACE, "xl_" + subjGrpLocalNameSuffix);
			Literal literalForm = vf.createLiteral(subjectGroupName, Locale.GERMAN.getLanguage());
			connection.add(subjGrIri, SKOSXL.PREF_LABEL, xLabelIri, baseURI);
			connection.add(xLabelIri, RDF.TYPE, SKOSXL.LABEL, baseURI);
			connection.add(xLabelIri, SKOSXL.LITERAL_FORM, literalForm, baseURI);
		}
	}
	
	private void enrichCluster(IRI concept, Term term) {
		String cluster = term.getCluster();
		if (cluster != null) {
			cluster = cluster.replaceAll(" ", "_");
			IRI clusterIri = vf.createIRI(GSOnto.NAMESPACE, cluster);
			connection.add(concept, SKOS.IN_SCHEME, clusterIri, baseURI);
			connection.add(clusterIri, RDF.TYPE, SKOS.CONCEPT_SCHEME, baseURI);
			connection.add(clusterIri, RDF.TYPE, GSVocab.CLUSTER, baseURI);
		}
	}
	
	private void enrichTargetGroup(IRI concept, Term term) {
		TargetGroup targetGroup = term.getTargetGroup();
		if (targetGroup == TargetGroup.A) {
			connection.add(concept, GSVocab.TARGET_GROUP, GSVocab.TARGET_GROUP_A, baseURI);
		} else if (targetGroup == TargetGroup.B) {
			connection.add(concept, GSVocab.TARGET_GROUP, GSVocab.TARGET_GROUP_B, baseURI);
		} else if (targetGroup == TargetGroup.C) {
			connection.add(concept, GSVocab.TARGET_GROUP, GSVocab.TARGET_GROUP_C, baseURI);
		} else if (targetGroup == TargetGroup.N) {
			connection.add(concept, GSVocab.TARGET_GROUP, GSVocab.TARGET_GROUP_N, baseURI);
		}
	}
	
	private void enrichTermHoursValue(IRI concept, Term term) {
		//<concept> gs:hours <if 0 Öffnungszeiten if 1 Sprechzeiten> instances of Hours
		String hoursValue = term.getTermHoursValue();
		if (hoursValue != null && hoursValue.equals("0")) {
			connection.add(concept, GSVocab.HOURS, GSVocab.HOURS_OFFNUNGSZEITEN, baseURI);
		} else if (hoursValue != null && hoursValue.equals("1")) {
			connection.add(concept, GSVocab.HOURS, GSVocab.HOURS_SPRECHZEITEN, baseURI);
		}
	}
	private void enrichTermCreatedDate(IRI resource, Term term) {
		//dct:created
		String createdDate = term.getTermCreatedDate();
		if (createdDate != null) {
			SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");
			try {
				Date date = parser.parse(createdDate);
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(date);
				XMLGregorianCalendar gregorianDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				Literal dateLiteral = vf.createLiteral(gregorianDate);
				connection.add(resource, DCTERMS.CREATED, dateLiteral, baseURI);
			} catch (ParseException | DatatypeConfigurationException e) {
				System.out.println("Unable to parse date " + createdDate);
				e.printStackTrace();
			}
		}
	}
	
	private void enrichTermModifiedDate(IRI resource, Term term) {
		//dct:modified
		String modifiedDate = term.getTermModifiedDate();
		if (modifiedDate != null) {
			SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");
			try {
				Date date = parser.parse(modifiedDate);
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(date);
				XMLGregorianCalendar gregorianDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				Literal dateLiteral = vf.createLiteral(gregorianDate);
				connection.add(resource, DCTERMS.MODIFIED, dateLiteral, baseURI);
			} catch (ParseException | DatatypeConfigurationException e) {
				System.out.println("Unable to parse date " + modifiedDate);
				e.printStackTrace();
			}
		}
	}
	
	private void enrichNotes(IRI concept, Term term) {
		/* <concept> skos:definition <value>
		 * <concept> skos:historyNote <value>
		 * <concept> skos:scopeNote <value> (not used so far, but should be included in new data model)
		 */
		List<TermNote> notes = term.getTermNotes();
		for (TermNote note : notes) {
			Literal noteLiteral = vf.createLiteral(note.getNote());
			if (note.getLabel() == NoteLabel.Definition) {
				connection.add(concept, SKOS.DEFINITION, noteLiteral, baseURI);
			} else if (note.getLabel() == NoteLabel.History) {
				connection.add(concept, SKOS.HISTORY_NOTE, noteLiteral, baseURI);
			} 
		}
	}
	
	private Collection<Term> getTopicOfSubtopic(TermEntity subtopicTerm) {
		//N for special case of 1053516
		if (subtopicTerm.getObjectType() == ObjectType.R || subtopicTerm.getObjectType() == ObjectType.N) {
			return subTopicToTopicsMap.get(subtopicTerm.getTermId());
		} else { //else wrong objectType: the subtopicTerm is not subtopic
			System.out.println("Wrong Subtopic term: " + subtopicTerm.getTermId() + " objectType: " + subtopicTerm.getObjectType());
			return null;
		}
	}
	
	/**
	 * Returns the term for which the given entity represents the altLabel
	 * @param entity
	 * @return
	 */
	private TermEntity getTermOfAltLabel(TermEntity entity) {
		Term term = zThes.getTermById(entity.getTermId());
		for (Relation r : term.getRelations()) {
			if (r.getRelationType() == RelationType.BS) {
				return zThes.getTermById(r.getTermId());
			}
		}
		return null;
	}
	
	/**
	 * Creates and adds a Concept from the given term. Returns the Concept
	 * @param term
	 * @return
	 */
	private IRI createConcept(TermEntity term) {
		IRI concept = vf.createIRI(GSOnto.NAMESPACE, "c_" + term.getTermId());
		connection.add(concept, RDF.TYPE, SKOS.CONCEPT, baseURI);
		IRI prefLabel = createXLabel(term);
		connection.add(concept, SKOSXL.PREF_LABEL, prefLabel, baseURI);
		return concept;
	}
	
	/**
	 * Creates and adds a ConcpetScheme from the given term. Returns the ConceptScheme
	 * @param term
	 * @return
	 */
	private IRI createConceptScheme(TermEntity term) {
		IRI scheme = vf.createIRI(GSOnto.NAMESPACE, "conceptScheme_" + term.getTermId());
		connection.add(scheme, RDF.TYPE, SKOS.CONCEPT_SCHEME, baseURI);
		IRI prefLabel = createXLabel(term);
		connection.add(scheme, SKOSXL.PREF_LABEL, prefLabel, baseURI);
		return scheme;
	}
	
	/**
	 * Creates and adds a gs:AccessEntry instance from the given term. Returns the instance
	 * @param term
	 * @return
	 */
	private IRI createAccessEntry(TermEntity term) {
		IRI accessEntry = vf.createIRI(GSOnto.NAMESPACE, "a_" + term.getTermId());
		connection.add(accessEntry, RDF.TYPE, GSVocab.ACCESS_ENTRY, baseURI);
		IRI prefLabel = createXLabel(term);
		connection.add(accessEntry, SKOSXL.PREF_LABEL, prefLabel, baseURI);
		return accessEntry;
	}
	
	/**
	 * Creates and adds a gs:AccessRelationship instance from the given term. Returns the relation
	 * @param term
	 * @return
	 */
	private IRI createAccessRelationship(TermEntity sourceTerm, TermEntity targetTerm) {
		IRI accessRelationship = vf.createIRI(GSOnto.NAMESPACE, "ar_" + sourceTerm.getTermId() + "_" + targetTerm.getTermId());
		connection.add(accessRelationship, RDF.TYPE, GSVocab.ACCESS_RELATIONSHIP, baseURI);
		return accessRelationship;
	}
	
	/**
	 * Creates and adds a gs:ControlledKeywordRelationship instance from the given term. Returns the relation
	 * @param term
	 * @return
	 */
	private IRI createControlledKeywordRelationship(TermEntity sourceTerm, TermEntity targetTerm) {
		IRI controlledKeywordRelationship = vf.createIRI(GSOnto.NAMESPACE, "kcr_" + sourceTerm.getTermId() + "_" + targetTerm.getTermId());
		connection.add(controlledKeywordRelationship, RDF.TYPE, GSVocab.CONTROLLED_KEYWORD_RELATIONSHIP, baseURI);
		return controlledKeywordRelationship;
	}
	
	/**
	 * Creates and adds an xLabel from termName of the given term. Returns the xLabel
	 * @param term
	 * @return
	 */
	private IRI createXLabel(TermEntity term) {
		IRI xLabel = getXLabelIRI(term);
		Literal literalForm = createLiteral(term);
		connection.add(xLabel, RDF.TYPE, SKOSXL.LABEL, baseURI);
		connection.add(xLabel, SKOSXL.LITERAL_FORM, literalForm, baseURI);
		return xLabel;
	}
	
	private IRI getXLabelIRI(TermEntity term) {
		return vf.createIRI(GSOnto.NAMESPACE, "xl_" + getNormalizedTermLanguage(term) + "_" + term.getTermId());
	}
	
	private Literal createLiteral(TermEntity term) {
		Literal literal = vf.createLiteral(term.getTermName(), getNormalizedTermLanguage(term));
		return literal;
	}
	
	private boolean isRelationCompliant(Term term, Relation relation) {
		if (relation.getRelationType() == RelationType.BZT || relation.getRelationType() == RelationType.ZTB) {
			return true; //relations to ignore
		}
		//special cases (see RelationType.QSP and RelationType.ZSP)
		else if (relation.getRelationType() == RelationType.QSP) { //D->D or D->A
			return (
				term.getTermType() == TermType.D && 
				(relation.getTermType() == TermType.A || relation.getTermType() == TermType.D) 
			);
		} else if (relation.getRelationType() == RelationType.ZSP) { //D->D or A->D
			return (
				(term.getTermType() == TermType.A || term.getTermType() == TermType.D) &&
				relation.getTermType() == TermType.D
			);
		}
		RelationType relationType = relation.getRelationType();
		TermType expectedFrom = relationType.getTermTypeFrom();
		TermType expectedTo = relationType.getTermTypeTo();
		return term.getTermType().subsumes(expectedFrom) && relation.getTermType().subsumes(expectedTo);
	}
	
	private String getNormalizedTermLanguage(TermEntity entity) {
		return new Locale(entity.getTermLanguage()).getLanguage();
	}
	
	private void logUnhandledRelation(Term term, Relation relation) {
		System.out.println("Unhandled " + relation.getRelationType() + " relation for " + 
				"objectType " + term.getObjectType() + "(" + term.getTermId() + " -> " + relation.getTermId() + ")");
	}
	
	private void logUnhanldedRelationForWrongObjectType(Term term, Relation relation) {
		wrongObjectTypeRelations.put(relation.getRelationType(), term.getTermId() + "(" + term.getObjectType() + ") -> " + relation.getTermId() + "(" + relation.getObjectType() + ")");
	}
	
	private void logUnhanldedRelationForWrongTermType(Term term, Relation relation) {
		wrongTermTypeRelations.put(relation.getRelationType(), term.getTermId() + "(" + term.getTermType() + ") -> " + relation.getTermId() + "(" + relation.getTermType() + ")");
	}

	public void export(File file, RDFFormat format) throws IOException {
		OutputStream os = new FileOutputStream(file);
		RDFWriter writer = Rio.createWriter(format, os);
		writer.set(BasicWriterSettings.PRETTY_PRINT, true);
		connection.exportStatements(null, null, null, false, writer);
	}
	
}
