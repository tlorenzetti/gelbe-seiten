package it.uniroma2.art.gelbeseiten;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.xml.sax.SAXException;

import it.uniroma2.art.gelbeseiten.exception.SerializationException;
import it.uniroma2.art.gelbeseiten.mapper.RdfToZthesMapper;
import it.uniroma2.art.gelbeseiten.mapper.ZthesToRdfMapper;
import it.uniroma2.art.gelbeseiten.model.ObjectSubtype;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Relation;
import it.uniroma2.art.gelbeseiten.model.RelationType;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermEntity;
import it.uniroma2.art.gelbeseiten.model.TermNote;
import it.uniroma2.art.gelbeseiten.model.TermType;
import it.uniroma2.art.gelbeseiten.model.Zthes;
import it.uniroma2.art.gelbeseiten.reader.XmlReader;
import it.uniroma2.art.gelbeseiten.serializer.TextSerializer;

public class Test {
	
//	private static File xmlFile = new File("C:\\Users\\Tiziano\\Desktop\\Gelbe Seiten\\input\\2018_02_06 Gelbe Seiten Thesaurus.xml");
	private static File xmlFile = new File("C:\\Users\\Tiziano\\Desktop\\Gelbe Seiten\\2018_04_25 Branchenthesaurus Gelbe Seiten\\2018_04_25 Branchenthesaurus Gelbe Seiten.xml");
	private static File rdfFile = new File("C:\\Users\\Tiziano\\Desktop\\Gelbe Seiten\\export.nt");
	private static File txtFile = new File("C:\\Users\\Tiziano\\Desktop\\Gelbe Seiten\\export.txt");
	private static File reportNoRelationFile = new File("C:\\Users\\Tiziano\\Desktop\\Gelbe Seiten\\reportNoRelation.txt");
	private static File reportNotDefinedFile = new File("C:\\Users\\Tiziano\\Desktop\\Gelbe Seiten\\reportNoDefined.txt");
	
	private static RDFFormat outputRdfFormat = RDFFormat.NTRIPLES;

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, SerializationException {
		
//		Zthes zthes = getZthesFromXml();
//		printStatisticsOnFile(zthes, new File("C:\\Users\\Tiziano\\Desktop\\statsFromXml.txt"));
		
		Zthes zthes = getZthesFromRdf();
		printStatisticsOnFile(zthes, new File("C:\\Users\\Tiziano\\Desktop\\statsFromRdf.txt"));
		
		
		
		/**
		 * Checked:
		 * T: ok
		 * R:
		 * B:
		 * Q:
		 * M: differences in relations with B
		 * NA: ok
		 * N: differences in relations with B
		 */
//		for (Term termFromXml : zThesFromXml.getTerms()) {
//			Term termFromRdf = zThesFromRdf.getTermById(termFromXml.getTermId());
//			if (termFromRdf == null) {
//				System.out.println("term " + termFromXml.getTermId() + " not found in rdf export");
//				continue;
//			}
//			compare(termFromXml, termFromRdf);
//		}
		
//		ZthesToRdfMapper rdfMapper = new ZthesToRdfMapper(zThes);
//		rdfMapper.map();
		
		
		/**
		 * REPORT OF TERMS WITHOUT RELATIONS AND RELATED TERMS NOT DEFINED
		 */
		
		//print on console
		
//		List<TermEntity> notFoundTerms = new ArrayList<>();
//		List<TermEntity> noRelationsTerms = new ArrayList<>();
//		for (Term t : zThesFromXml.getTerms()) {
//			List<Relation> rels = t.getRelations();
//			if (rels.isEmpty()) {
//				noRelationsTerms.add(t);
//			}
//			for (Relation r : rels) {
//				if (r.getObjectType() == ObjectType.Z) continue;
//				Term relatedTerm = zThesFromXml.getTermById(r.getTermId());
//				if (relatedTerm == null) {
//					boolean alreadyIn = false;
//					for (TermEntity nft : notFoundTerms) {
//						if (nft.getTermId().equals(r.getTermId())) {
//							alreadyIn = true;
//							break;
//						}
//					}
//					if (!alreadyIn) {
//						notFoundTerms.add(r);
//					}
//				}
//			}
//		}
//		System.out.println("broken relations: " + notFoundTerms.size() + " " + notFoundTerms);
//		System.out.println("noRelationsTerms " + noRelationsTerms.size() + " " + noRelationsTerms);
		
		//Report on file
		
//		Zthes zThesFromXml = getZthesFromXml();
//		try (
//				BufferedWriter bwNotDefined = new BufferedWriter(new FileWriter(reportNotDefinedFile));
//				BufferedWriter bwNoRelation = new BufferedWriter(new FileWriter(reportNoRelationFile));
//		) {
//			for (Term t : zThesFromXml.getTerms()) {
//				List<Relation> rels = t.getRelations();
//				if (rels.isEmpty()) {
//					bwNoRelation.write("Term without relation: " + t.getTermId() + "(" + t.getObjectType() + ")");
//					bwNoRelation.newLine();
//				} else {
//					for (Relation r : rels) {
//						if (r.getObjectType() == ObjectType.Z) continue;
//						Term relatedTerm = zThesFromXml.getTermById(r.getTermId());
//						if (relatedTerm == null) {
//							bwNotDefined.write("Missing rel term: " + t.getTermId() + "(" + t.getObjectType() + ") -" + r.getRelationType() + "-> " + r.getTermId() + "(" + r.getObjectType() + ")");
//							bwNotDefined.newLine();
//						}
//					}
//				}
//			}
//		}
		
		//Rubrik (Subtopic - top concept) not defined and never related to any topic
		
//		for (Term t : zThesFromXml.getTerms()) {
//			List<Relation> rels = t.getRelations();
//			for (Relation r : rels) {
//				Term relatedTerm = zThesFromXml.getTermById(r.getTermId());
//				if (relatedTerm == null && r.getObjectType() == ObjectType.R) {
//					//look if the the subtopic is related to a topic
//					boolean found = false;
//					loop: for (Term topic : zThesFromXml.getTermsByObjectType(ObjectType.T)) {
//						for (Relation topicRel : topic.getRelations()) {
//							if (topicRel.getTermId().equals(r.getTermId())) {
//								found = true;
//								break loop;
//							}
//						}
//					}
//					if (!found) {
//						System.out.println("Subtopic " + r.getTermId() + " not defined and never in relation with any topic " + r.getAppName());
//					}
//				}
//			}
//		}
		
	}
	
	private static void compare(Term termXml, Term termRdf) {
		if (termXml.getAppName() != null && termXml.getAppName() != termRdf.getAppName()) {
			System.out.println("different appName: " + termXml.getTermId());
		}
		if (termXml.getObjectType() != null && termXml.getObjectType() != termRdf.getObjectType()) {
			System.out.println("different getObjectType: " + termXml.getTermId());
		}
		//ignore cluster, objectSubtype and subjectGroup since they are omitted for termType N and for not pivotTerm
		if (termXml.getCluster() != null && !termXml.getCluster().equals(termRdf.getCluster())) {
			System.out.println("different cluster: " + termXml.getTermId());
		}
		if (termXml.getObjectSubtype() != null && termXml.getObjectSubtype() != termRdf.getObjectSubtype()) {
			System.out.println("different getObjectSubtype: " + termXml.getTermId());
		}
		if (termXml.getSubjectGroup() != null && !termXml.getSubjectGroup().equals(termRdf.getSubjectGroup())) {
			System.out.println("different getSubjectGroup: " + termXml.getTermId());
		}
		if (termXml.getSubjectGroupName() != null && !termXml.getSubjectGroupName().equals(termRdf.getSubjectGroupName())) {
			System.out.println("different getSubjectGroupName: " + termXml.getTermId());
		}
		if (termXml.getTargetGroup() != null && termXml.getTargetGroup() != termRdf.getTargetGroup()) {
			System.out.println("different getTargetGroup: " + termXml.getTermId());
		}
		if (termXml.getTermCreatedDate() != null && !termXml.getTermCreatedDate().equals(termRdf.getTermCreatedDate())) {
			System.out.println("different getTermCreatedDate: " + termXml.getTermId());
		}
		if (termXml.getTermDisplayStatus() != null && !termXml.getTermDisplayStatus().equals(termRdf.getTermDisplayStatus())) {
			System.out.println("different getTermDisplayStatus: " + termXml.getTermId());
		}
		if (termXml.getTermFacet() != null && termXml.getTermFacet() != termRdf.getTermFacet()) {
			System.out.println("different getTermFacet: " + termXml.getTermId());
		}
		if (termXml.getTermGrammar() != null && termXml.getTermGrammar() != termRdf.getTermGrammar()) {
			System.out.println("different getTermGrammar: " + termXml.getTermId());
		}
		if (termXml.getTermHoursValue() != null && !termXml.getTermHoursValue().equals(termRdf.getTermHoursValue())) {
			System.out.println("different getTermHoursValue: " + termXml.getTermId());
		}
		if (termXml.getTermLanguage() != null && !termXml.getTermLanguage().equals(termRdf.getTermLanguage())) {
			System.out.println("different getTermLanguage: " + termXml.getTermId());
		}
		if (termXml.getTermModifiedDate() != null && !termXml.getTermModifiedDate().equals(termRdf.getTermModifiedDate())) {
			System.out.println("different getTermModifiedDate: " + termXml.getTermId());
		}
		if (termXml.getTermName() != null && !termXml.getTermName().equals(termRdf.getTermName())) {
			System.out.println("different getTermName: " + termXml.getTermId());
		}
		if (termXml.getTermQualifier() != null && !termXml.getTermQualifier().equals(termRdf.getTermQualifier())) {
			System.out.println("different getTermQualifier: " + termXml.getTermId());
		}
		if (termXml.getTermStatus() != null && !termXml.getTermStatus().equals(termRdf.getTermStatus())) {
			System.out.println("different getTermStatus: " + termXml.getTermId());
		}
		
		for (TermNote noteXml : termXml.getTermNotes()) {
			boolean found = false;
			for (TermNote noteRdf : termRdf.getTermNotes()) {
				if (noteXml.getNote().equals(noteRdf.getNote()) && noteXml.getLabel() == noteRdf.getLabel()) {
					found = true;
					break;
				}
			}
			if (!found) {
				System.out.println("different getTermNotes: " + termXml.getTermId());
			}
		}
		
//		for (Relation relXml : termXml.getRelations()) {
//			boolean found = false;
//			for (Relation relRdf : termRdf.getRelations(relXml.getRelationType())) {
//				if (relXml.getTermId().equals(relRdf.getTermId())) {
//					if (compareTermEntity(relXml, relRdf)) {
//						found = true;
//						break;
//					}
//				}
//			}
//			if (!found) {
//				System.out.println("different relation: " 
//						+ termXml.getObjectType() + " " + termXml.getTermId() + " " + relXml.getRelationType() + " " 
//						+ relXml.getObjectType() + " " + relXml.getTermId());
//			}
//		}
	}
	
	private static Zthes getZthesFromXml() throws ParserConfigurationException, SAXException, IOException {
		XmlReader reader = new XmlReader(xmlFile);
		reader.initialize();
		Zthes zThesFromXml = reader.parseZThes();
		return zThesFromXml;
	}
	
	private static Zthes getZthesFromRdf() throws RDFParseException, RepositoryException, IOException {
		RdfToZthesMapper rdfToZts = new RdfToZthesMapper();
		rdfToZts.initialize(rdfFile);
		Zthes zThesFromRdf = rdfToZts.map();
		return zThesFromRdf;
	}
	
	private static boolean compareTermEntity(TermEntity e1, TermEntity e2) {
		if (e1.getAppName() != e2.getAppName()) {
			System.out.println("different getAppName");
		}
		if (e1.getObjectType() != e2.getObjectType()) {
			System.out.println("different getObjectType");
		}
		if (!e1.getTermId().equals(e2.getTermId())) {
			System.out.println("different getTermId");
		}
		if (!e1.getTermLanguage().equals(e2.getTermLanguage())) {
			System.out.println("different getTermLanguage");
		}
		if (!e1.getTermName().equals(e2.getTermName())) {
			System.out.println("different getTermName");
		}
		if (e1.getTermQualifier() != null && !e1.getTermQualifier().equals(e2.getTermQualifier())) {
			System.out.println("different getTermQualifier");
		}
		if (!e1.getTermType().equals(e2.getTermType())) {
			System.out.println("different getTermType");
		}
		
		return (
				e1.getAppName() == e2.getAppName() &&
				e1.getObjectType() == e2.getObjectType() &&
				e1.getTermId().equals(e2.getTermId()) &&
				e1.getTermLanguage().equals(e2.getTermLanguage()) &&
				e1.getTermName().equals(e2.getTermName()) &&
				e1.getTermQualifier().equals(e2.getTermQualifier()) &&
				e1.getTermType().equals(e2.getTermType())
		);
	}
	
	
	private static void printNumbersOnConsole(Zthes zThes) {
		List<ObjectType> objTypes = Arrays.asList(ObjectType.T, ObjectType.R, ObjectType.B, ObjectType.M, ObjectType.Q, ObjectType.NA, ObjectType.N);
		for (ObjectType ot : objTypes) {
			System.out.println("collecting stats for " + ot);
			Collection<Term> terms = zThes.getTermsByObjectType(ot);
			
			int termD_Count = 0;
			List<Term> termD_DE = new ArrayList<>();
			List<Term> termD_EN = new ArrayList<>();
			List<Term> termD_FR = new ArrayList<>();
			int termN_Count = 0;
			List<Term> termN_DE = new ArrayList<>();
			List<Term> termN_EN = new ArrayList<>();
			List<Term> termN_FR = new ArrayList<>();
			int termA_Count = 0;
			List<Term> termA_DE = new ArrayList<>();
			List<Term> termA_EN = new ArrayList<>();
			List<Term> termA_FR = new ArrayList<>();
			int termK_Count = 0;
			List<Term> termK_DE = new ArrayList<>();
			List<Term> termK_EN = new ArrayList<>();
			List<Term> termK_FR = new ArrayList<>();
			for (Term t : terms) {
				if (t.getTermType() == TermType.D) {
					termD_Count++;
					if (t.getTermLanguage().equals("DE")) {
						termD_DE.add(t);
					} else if (t.getTermLanguage().equals("EN")) {
						termD_EN.add(t);
					} else if (t.getTermLanguage().equals("FR")) {
						termD_FR.add(t);
					}
				} else if (t.getTermType() == TermType.N) {
					termN_Count++;
					if (t.getTermLanguage().equals("DE")) {
						termN_DE.add(t);
					} else if (t.getTermLanguage().equals("EN")) {
						termN_EN.add(t);
					} else if (t.getTermLanguage().equals("FR")) {
						termN_FR.add(t);
					}
				} else if (t.getTermType() == TermType.A) {
					termA_Count++;
					if (t.getTermLanguage().equals("DE")) {
						termA_DE.add(t);
					} else if (t.getTermLanguage().equals("EN")) {
						termA_EN.add(t);
					} else if (t.getTermLanguage().equals("FR")) {
						termA_FR.add(t);
					}
				} else if (t.getTermType() == TermType.K) {
					termK_Count++;
					if (t.getTermLanguage().equals("DE")) {
						termK_DE.add(t);
					} else if (t.getTermLanguage().equals("EN")) {
						termK_EN.add(t);
					} else if (t.getTermLanguage().equals("FR")) {
						termK_FR.add(t);
					}
				}
			}
			System.out.println("terms " + ot + " " + terms.size());
			System.out.println("terms " + ot + " D " + termD_Count);
			System.out.println("terms " + ot + " D DE " + termD_DE.size());
			System.out.println("terms " + ot + " D EN " + termD_EN.size());
			System.out.println("terms " + ot + " D FR " + termD_FR.size());
			System.out.println("terms " + ot + " N " + termN_Count);
			System.out.println("terms " + ot + " N DE " + termN_DE.size());
			System.out.println("terms " + ot + " N EN " + termN_EN.size());
			System.out.println("terms " + ot + " N FR " + termN_FR.size());
			System.out.println("terms " + ot + " A " + termA_Count);
			System.out.println("terms " + ot + " A DE " + termA_DE.size());
			System.out.println("terms " + ot + " A EN " + termA_EN.size());
			System.out.println("terms " + ot + " A FR " + termA_FR.size());
			System.out.println("terms " + ot + " K " + termK_Count);
			System.out.println("terms " + ot + " K DE " + termK_DE.size());
			System.out.println("terms " + ot + " K EN " + termK_EN.size());
			System.out.println("terms " + ot + " K FR " + termK_FR.size());
			
		}
	}
	
	private static void printStatisticsOnFile(Zthes zThes, File file) throws IOException {
		List<ObjectType> objTypes = Arrays.asList(ObjectType.T, ObjectType.R, ObjectType.B, ObjectType.M, ObjectType.Q, ObjectType.NA, ObjectType.N);
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (ObjectType ot : objTypes) {
				System.out.println("collecting stats for " + ot);
				Collection<Term> terms = zThes.getTermsByObjectType(ot);
				
				int termD_Count = 0;
				List<Term> termD_DE = new ArrayList<>();
				List<Term> termD_EN = new ArrayList<>();
				List<Term> termD_FR = new ArrayList<>();
				int termN_Count = 0;
				List<Term> termN_DE = new ArrayList<>();
				List<Term> termN_EN = new ArrayList<>();
				List<Term> termN_FR = new ArrayList<>();
				int termA_Count = 0;
				List<Term> termA_DE = new ArrayList<>();
				List<Term> termA_EN = new ArrayList<>();
				List<Term> termA_FR = new ArrayList<>();
				int termK_Count = 0;
				List<Term> termK_DE = new ArrayList<>();
				List<Term> termK_EN = new ArrayList<>();
				List<Term> termK_FR = new ArrayList<>();
				for (Term t : terms) {
					if (t.getTermType() == TermType.D) {
						termD_Count++;
						if (t.getTermLanguage().equals("DE")) {
							termD_DE.add(t);
						} else if (t.getTermLanguage().equals("EN")) {
							termD_EN.add(t);
						} else if (t.getTermLanguage().equals("FR")) {
							termD_FR.add(t);
						}
					} else if (t.getTermType() == TermType.N) {
						termN_Count++;
						if (t.getTermLanguage().equals("DE")) {
							termN_DE.add(t);
						} else if (t.getTermLanguage().equals("EN")) {
							termN_EN.add(t);
						} else if (t.getTermLanguage().equals("FR")) {
							termN_FR.add(t);
						}
					} else if (t.getTermType() == TermType.A) {
						termA_Count++;
						if (t.getTermLanguage().equals("DE")) {
							termA_DE.add(t);
						} else if (t.getTermLanguage().equals("EN")) {
							termA_EN.add(t);
						} else if (t.getTermLanguage().equals("FR")) {
							termA_FR.add(t);
						}
					} else if (t.getTermType() == TermType.K) {
						termK_Count++;
						if (t.getTermLanguage().equals("DE")) {
							termK_DE.add(t);
						} else if (t.getTermLanguage().equals("EN")) {
							termK_EN.add(t);
						} else if (t.getTermLanguage().equals("FR")) {
							termK_FR.add(t);
						}
					}
				}
			
				bw.write(ot + " D count: " + termD_Count);
				bw.newLine();
				bw.write(ot + " D DE count: " + termD_DE.size());
				bw.newLine();
				for (Term t : termD_DE) {
					bw.write(ot + " D DE " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " D EN count: " + termD_EN.size());
				bw.newLine();
				for (Term t : termD_EN) {
					bw.write(ot + " D EN " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " D FR count: " + termD_FR.size());
				bw.newLine();
				for (Term t : termD_FR) {
					bw.write(ot + " D FR " + t.getTermId());
					bw.newLine();
				}
				
				bw.write(ot + " N count: " + termN_Count);
				bw.newLine();
				bw.write(ot + " N DE count: " + termN_DE.size());
				bw.newLine();
				for (Term t : termN_DE) {
					bw.write(ot + " N DE " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " N EN count: " + termN_EN.size());
				bw.newLine();
				for (Term t : termN_EN) {
					bw.write(ot + " N EN " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " N FR count: " + termN_FR.size());
				bw.newLine();
				for (Term t : termN_FR) {
					bw.write(ot + " N FR " + t.getTermId());
					bw.newLine();
				}
				
				bw.write(ot + " A count: " + termA_Count);
				bw.newLine();
				bw.write(ot + " A DE count: " + termA_DE.size());
				bw.newLine();
				for (Term t : termA_DE) {
					bw.write(ot + " A DE " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " A EN count: " + termA_EN.size());
				bw.newLine();
				for (Term t : termA_EN) {
					bw.write(ot + " A EN " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " A FR count: " + termA_FR.size());
				bw.newLine();
				for (Term t : termA_FR) {
					bw.write(ot + " A FR " + t.getTermId());
					bw.newLine();
				}
				
				bw.write(ot + " K count: " + termK_Count);
				bw.newLine();
				bw.write(ot + " K DE count: " + termK_DE.size());
				bw.newLine();
				for (Term t : termK_DE) {
					bw.write(ot + " K DE " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " K EN count: " + termK_EN.size());
				bw.newLine();
				for (Term t : termK_EN) {
					bw.write(ot + " K EN " + t.getTermId());
					bw.newLine();
				}
				bw.write(ot + " K FR count: " + termK_FR.size());
				bw.newLine();
				for (Term t : termK_FR) {
					bw.write(ot + " K FR " + t.getTermId());
					bw.newLine();
				}
			}
		}
	} 

}
