package it.uniroma2.art.gelbeseiten.reader;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.uniroma2.art.gelbeseiten.model.AppName;
import it.uniroma2.art.gelbeseiten.model.ObjectSubtype;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Relation;
import it.uniroma2.art.gelbeseiten.model.RelationType;
import it.uniroma2.art.gelbeseiten.model.TargetGroup;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermFacet;
import it.uniroma2.art.gelbeseiten.model.TermGrammar;
import it.uniroma2.art.gelbeseiten.model.TermNote;
import it.uniroma2.art.gelbeseiten.model.TermNote.NoteLabel;
import it.uniroma2.art.gelbeseiten.model.TermStatus;
import it.uniroma2.art.gelbeseiten.model.TermType;
import it.uniroma2.art.gelbeseiten.model.Zthes;

public class XmlReader {
	
	private File file;
	private Document doc;
	
	public XmlReader(File file) {
		this.file = file;
	}
	
	public void initialize() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();
	}
	
	public Zthes parseZThes() {
		Zthes zthes = new Zthes();
		Element zthesElement = doc.getDocumentElement();
//		if (!zthesElement.getNodeName().equals(Zthes.Tag.ZTHES)) {
//			logger.warn("Unknown root element " + zthesElement.getNodeName() + ". " + Zthes.Tag.ZTHES + " expected");
//		}
		
		NodeList zthesChildNodes = zthesElement.getChildNodes();
		for (int i = 0; i < zthesChildNodes.getLength(); i++) {
			Node childNode = zthesChildNodes.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				Element childElement = (Element) childNode;
				if (childElement.getNodeName().equals(Term.Tag.TERM)) { //the only accepted child element of <zthes> is <term>
					Term term = parseTerm(childElement);
					zthes.addTerm(term);
				} else {
//					logger.warn("Unknown child element of " + Zthes.Tag.ZTHES + " element: '" +
//							childElement.getNodeName() + "'. Element ignored.");
				}
			}
		}
		return zthes;
	}
	
	public Term parseTerm(Element termElement) {
		Term term = null;
		//mandatory
		String termId = null;
		ObjectType objectType = null;
		String termName = null; 
		//optional
		AppName appName = null;
		String cluster = null;
		ObjectSubtype objectSubtype = null;
		List<Relation> relations = new ArrayList<Relation>();
		String subjectGroup = null;
		String subjectGroupName = null;
		TargetGroup targetGroup = null;
		String termCreatedBy = null;
		String termCreatedDate = null;
		String termDisplayStatus = null;
		TermFacet termFacet = null;
		TermGrammar termGrammar = null;
		String termHoursValue = null;
		String termLanguage = null;
		String termModifiedBy = null;
		String termModifiedDate = null;
		List<TermNote> termNotes = new ArrayList<TermNote>();
		String termQualifier = null;
		TermStatus termStatus = null;
		TermType termType = null;
		NodeList termChildNodes = termElement.getChildNodes();
		for (int i = 0; i < termChildNodes.getLength(); i++) {
			Node childNode = termChildNodes.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				Element childElement = (Element) childNode;
				if (childElement.getNodeName().equals(Term.Tag.APP_NAME)) {
					appName = AppName.fromString(childElement.getTextContent());
				} else if (childElement.getNodeName().equals(Term.Tag.CLUSTER)) {
					cluster = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.OBJECT_SUBTYPE)) {
					objectSubtype = ObjectSubtype.valueOf(childElement.getTextContent().toUpperCase());
				} else if (childElement.getNodeName().equals(Term.Tag.OBJECT_TYPE)) {
					objectType = ObjectType.valueOf(childElement.getTextContent());
				} else if (childElement.getNodeName().equals(Term.Tag.RELATION)) {
					relations.add(parseRelation(childElement));
				} else if (childElement.getNodeName().equals(Term.Tag.SUBJECT_GROUP)) {
					subjectGroup = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.SUBJECT_GROUP_NAME)) {
					subjectGroupName = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TARGET_GROUP)) {
					targetGroup = TargetGroup.valueOf(childElement.getTextContent().toUpperCase());
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_CREATED_BY)) {
					termCreatedBy = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_CREATED_DATE)) {
					termCreatedDate = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_DISPLAY_STATUS)) {
					termDisplayStatus = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_FACET)) {
					String tfValue = childElement.getTextContent();
					if (!tfValue.equals("")) {
						termFacet = TermFacet.valueOf(tfValue);
					}
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_GRAMMAR)) {
					termGrammar = TermGrammar.valueOf(childElement.getTextContent().toUpperCase());
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_HOURS_VALUE)) {
					termHoursValue = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_ID)) {
					termId = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_LANGUAGE)) {
					termLanguage = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_MODIFIED_BY)) {
					termModifiedBy = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_MODIFIED_DATE)) {
					termModifiedDate = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_NAME)) {
					termName = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_NOTE)) {
					termNotes.add(parseTermNote(childElement));
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_QUALIFIER)) {
					termQualifier = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_STATUS)) {
					String termStatusValue = childElement.getTextContent();
					if (!termStatusValue.equals("")) {
						termStatus = TermStatus.valueOf(termStatusValue);
					}
				} else if (childElement.getNodeName().equals(Term.Tag.TERM_TYPE)) {
					termType = TermType.valueOf(childElement.getTextContent());
				} else {
//					logger.warn("Unknown child element of " + Term.Tag.TERM + " element: '" +
//							childElement.getNodeName() + "'. Element ignored.");
				}
			}
		}
		if (termId != null && objectType != null && termName != null) {
			term = new Term(termId, objectType, termName);
			if (appName != null) {
				term.setAppName(appName);
			}
			if (cluster != null && !cluster.equals("")) {
				term.setCluster(cluster);
			}
			if (objectSubtype != null) {
				term.setObjectSubtype(objectSubtype);
			}
			if (relations != null) {
				term.setRelation(relations);
			}
			if (subjectGroup != null && !subjectGroup.equals("")) {
				term.setSubjectGroup(subjectGroup);
			}
			if (subjectGroupName != null && !subjectGroupName.equals("")) {
				term.setSubjectGroupName(subjectGroupName);
			}
			if (targetGroup != null) {
				term.setTargetGroup(targetGroup);
			}
			if (termCreatedBy != null && !termCreatedBy.equals("")) {
				term.setTermCreatedBy(termCreatedBy);
			}
			if (termCreatedDate != null && !termCreatedDate.equals("")) {
				term.setTermCreatedDate(termCreatedDate);
			}
			if (termDisplayStatus != null && !termDisplayStatus.equals("")) {
				term.setTermDisplayStatus(termDisplayStatus);
			}
			if (termFacet != null) {
				term.setTermFacet(termFacet);
			}
			if (termGrammar != null) {
				term.setTermGrammar(termGrammar);
			}
			if (termHoursValue != null && !termHoursValue.equals("")) {
				term.setTermHoursValue(termHoursValue);
			}
			if (termLanguage != null && !termLanguage.equals("")) {
				term.setTermLanguage(termLanguage);
			}
			if (termModifiedBy != null && !termModifiedBy.equals("")) {
				term.setTermModifiedBy(termModifiedBy);
			}
			if (termModifiedDate != null && !termModifiedDate.equals("")) {
				term.setTermModifiedDate(termModifiedDate);
			}
			if (termNotes != null) {
				term.setTermNotes(termNotes);
			}
			if (termQualifier != null && !termQualifier.equals("")) {
				term.setTermQualifier(termQualifier);
			}
			if (termStatus != null) {
				term.setTermStatus(termStatus);
			}
			if (termType != null) {
				term.setTermType(termType);
			}
		} else {
//			logger.warn("Mandatory child element not found of " + Term.Tag.TERM + " element");
		}
		return term;
	}
	

	
	public Relation parseRelation(Element relationElement) {
		Relation relation = null;
		//mandatory
		RelationType relationType = null;
		String termId = null;
		ObjectType objectType = null;
		String termName = null;
		//optional
		String sourceDb = null;
		String termQualifier = null;
		AppName appName = null;
		TermType termType = null;
		String termLanguage = null;
		float weight = 0.0f;
		
		//Attributes
		NamedNodeMap nodeAttrs = relationElement.getAttributes();
		for (int i = 0; i < nodeAttrs.getLength(); i++) {
			Node nodeAttr = nodeAttrs.item(i);
			if (nodeAttr.getNodeType() == Node.ATTRIBUTE_NODE) {
				Attr attr = (Attr) nodeAttr;
				if (attr.getName().equals(Relation.Attr.WEIGHT)) {
					DecimalFormat format = new DecimalFormat("0.#", DecimalFormatSymbols.getInstance(Locale.GERMAN));
					try {
						weight = format.parse(attr.getValue()).floatValue();
					} catch (ParseException e1) {}
				} else {
//					logger.warn("Unknown attribute of " + Term.Tag.RELATION + " element: '" +
//							attr.getName() + "'. Attribute ignored.");
				}
			}
		}
		//Elements
		NodeList relationChildNodes = relationElement.getChildNodes();
		for (int i = 0; i < relationChildNodes.getLength(); i++) {
			Node childNode = relationChildNodes.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				Element childElement = (Element) childNode;
				if (childElement.getNodeName().equals(Relation.Tag.APP_NAME)) {
					appName = AppName.fromString(childElement.getTextContent());
				} else if (childElement.getNodeName().equals(Relation.Tag.OBJECT_TYPE)) {
					objectType = ObjectType.valueOf(childElement.getTextContent());
				} else if (childElement.getNodeName().equals(Relation.Tag.RELATION_TYPE)) {
					relationType = RelationType.valueOf(childElement.getTextContent());
				} else if (childElement.getNodeName().equals(Relation.Tag.SOURCE_DB)) {
					sourceDb = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Relation.Tag.TERM_ID)) {
					termId = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Relation.Tag.TERM_LANGUAGE)) {
					termLanguage = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Relation.Tag.TERM_NAME)) {
					termName = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Relation.Tag.TERM_QUALIFIER)) {
					termQualifier = childElement.getTextContent();
				} else if (childElement.getNodeName().equals(Relation.Tag.TERM_TYPE)) {
					termType = TermType.valueOf(childElement.getTextContent());
				} else {
//					logger.warn("Unknown child element of " + Term.Tag.RELATION + " element: '" +
//							childElement.getNodeName() + "'. Element ignored.");
				}
			}
		}
		if (relationType != null && termId != null && objectType != null && termName != null) {
			relation = new Relation(relationType, termId, objectType, termName);
			if (sourceDb != null && !sourceDb.equals("")) {
				relation.setSourceDb(sourceDb);
			}
			if (termQualifier != null && !termQualifier.equals("")) {
				relation.setTermQualifier(termQualifier);
			}
			if (appName != null) {
				relation.setAppName(appName);
			}
			if (termType != null) {
				relation.setTermType(termType);
			}
			if (termLanguage != null && !termLanguage.equals("")) {
				relation.setTermLanguage(termLanguage);
			}
			if (weight != 0.0f) {
				relation.setWeight(weight);
			}
		} else {
			System.out.println("Mandatory child element not found of " + Term.Tag.RELATION + " element");
		}
		return relation;
	}
	
	public TermNote parseTermNote(Element termNoteElement) {
		TermNote termNote = new TermNote(termNoteElement.getTextContent());
		//Optional attributes
		NoteLabel label = null;
		String vocab = null;
		NamedNodeMap nodeAttrs = termNoteElement.getAttributes();
		for (int i = 0; i < nodeAttrs.getLength(); i++) {
			Node nodeAttr = nodeAttrs.item(i);
			if (nodeAttr.getNodeType() == Node.ATTRIBUTE_NODE) {
				Attr attr = (Attr) nodeAttr;
				if (attr.getName().equals(TermNote.Attr.LABEL)) {
					label = NoteLabel.valueOf(attr.getValue());
				} else if (attr.getName().equals(TermNote.Attr.VOCAB)) {
					vocab = attr.getValue();
				} else {
					System.out.println("Unknown attribute of " + Term.Tag.TERM_NOTE + " element: '" +
							attr.getName() + "'. Attribute ignored.");
				}
			}
		}
		if (label != null) {
			termNote.setLabel(label);
		}
		if (vocab != null) {
			termNote.setVocab(vocab);
		}
		return termNote;
	}
	
}
