package it.uniroma2.art.gelbeseiten;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriterRegistry;
import org.xml.sax.SAXException;

import it.uniroma2.art.gelbeseiten.mapper.ZthesToRdfMapper;
import it.uniroma2.art.gelbeseiten.model.Zthes;
import it.uniroma2.art.gelbeseiten.reader.XmlReader;

public class XmlToRdf {
	
	/**
	 * @param args args[0] input xml file, args[1] output rdf file (including extension)
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		if (args.length < 2) {
			System.err.println("\nuse two arguments:\n\t1) input xml file\n\t2) output rdf file");
			System.exit(0);
		}
		
		String inputFilePath = args[0];
		File inputFile = new File(inputFilePath);
		if (!inputFile.exists()) {
			System.err.println("\nFile " + inputFilePath + " not found.");
			System.exit(0);
		}
		
		String outputFilePath = args[1];
		File outputFile = new File(outputFilePath);
		String ext = FilenameUtils.getExtension(outputFilePath);
		RDFFormat outputRdfFormat = RDFWriterRegistry.getInstance().getKeys().stream()
				.filter(f -> f.getDefaultFileExtension().equals(ext))
				.findAny().orElse(null);
		if (outputRdfFormat == null) {
			System.err.println("\nFile " + outputFilePath + " has not a valid RDFFormat extention");
			System.exit(0);
		}
		
		System.out.print("Parsing xml input file (" + inputFile.getName() + ")... ");
		XmlReader reader = new XmlReader(inputFile);
		reader.initialize();
		Zthes zThes = reader.parseZThes();
		System.out.println("Done");
		
		ZthesToRdfMapper rdfMapper = new ZthesToRdfMapper(zThes);
		rdfMapper.map();
		
		System.out.print("Exporting... ");
		rdfMapper.export(outputFile, outputRdfFormat);
		System.out.println("Done");
		
	}

}
