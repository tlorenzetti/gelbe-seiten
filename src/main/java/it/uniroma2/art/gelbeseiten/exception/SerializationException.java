package it.uniroma2.art.gelbeseiten.exception;

public class SerializationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6015086539225428409L;

	public SerializationException(){
		super();
	}
	
	public SerializationException(String msg){
		super(msg);
	}

	public SerializationException(Throwable e){
		super(e);
	}

}
