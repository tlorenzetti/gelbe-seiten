package it.uniroma2.art.gelbeseiten.serializer;

import java.io.File;

import it.uniroma2.art.gelbeseiten.exception.SerializationException;
import it.uniroma2.art.gelbeseiten.model.Zthes;

public interface Serializer {
	
	public void serialize(Zthes zThes, File file) throws SerializationException;
	
}
