package it.uniroma2.art.gelbeseiten.serializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import it.uniroma2.art.gelbeseiten.exception.SerializationException;
import it.uniroma2.art.gelbeseiten.model.ObjectSubtype;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Relation;
import it.uniroma2.art.gelbeseiten.model.RelationType;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermStatus;
import it.uniroma2.art.gelbeseiten.model.TermType;
import it.uniroma2.art.gelbeseiten.model.Zthes;

public class TextSerializer implements Serializer {

	/**
	 * Serializes only Branches (ObjectType B) with termStatus "frei" or "gesperrt", descriptors and nondescriptors. 
	 * Main line containing termID, termType, objectSubtype, termStatus, termName
	 * If main line contains a nondescriptor it is followed by 1..n additonal lines prefixed by an arrow and details of the respective descriptors. 
	 */
	public void serialize(Zthes zThes, File file) throws SerializationException {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.ISO_8859_1))) {
				for (Term t : zThes.getTermsByObjectType(ObjectType.B)) {
					//Headings (objectType "Branche") in German only with termStatus "frei" or "gesperrt", descriptors and nondescriptors. 
					if (
						t.getTermLanguage().equals("DE") && 
						(t.getTermStatus() == TermStatus.frei || t.getTermStatus() == TermStatus.gesperrt)
					) {
						bw.write("      " + getTermIdFormatted(t));
						bw.write(" ");
						bw.write(t.getTermType().toString());
						bw.write(" ");
						bw.write(getObjectSubtypeFormatted(t));
						bw.write(" ");
						bw.write(" " + getTermStatusSerialization(t.getTermStatus()));
						bw.write(" ");
						bw.write(t.getTermName());
						if (t.getTermType() != TermType.D) { //2nd line
							for (Term d : getDescriptors(t, zThes)) {
								bw.write("\n              --> ");
								bw.write(getTermIdFormatted(d));
								bw.write(" ");
								bw.write(d.getTermType().toString());
								bw.write(" ");
								bw.write(getObjectSubtypeFormatted(d));
								bw.write(" ");
								bw.write(" " + getTermStatusSerialization(d.getTermStatus()));
								bw.write(" ");
								bw.write(d.getTermName());
							}
						}
						bw.write("\n");
					}
				}
			}
		} catch (IOException e) {
			throw new SerializationException(e);
		}
	}
	
	private List<Term> getDescriptors(Term termNonDesc, Zthes zThes) {
		List<Term> descriptors = new ArrayList<>(); 
		if (termNonDesc.getTermType() == TermType.N) {
			for (Relation rel: termNonDesc.getRelations(RelationType.BS)) {
				descriptors.add(zThes.getTermById(rel.getTermId()));
			}
		} else if (termNonDesc.getTermType() == TermType.A) {
			for (Relation rel: termNonDesc.getRelations(RelationType.BSA)) {
				descriptors.add(zThes.getTermById(rel.getTermId()));
			}
		} else if (termNonDesc.getTermType() == TermType.K) {
			for (Relation rel: termNonDesc.getRelations(RelationType.BK)) {
				descriptors.add(zThes.getTermById(rel.getTermId()));
			}
		}
		return descriptors;
	}
	
	/**
	 * leading zeroes if termID < 10000
	 * @param term
	 * @return
	 */
	private String getTermIdFormatted(Term term) {
		int idAsInt = Integer.parseInt(term.getTermId());
		return String.format("%05d", idAsInt);
	}
	
	/**
	 * Two chars, with trailing spaces if the subtype has only 1 char
	 * @param term
	 * @return
	 */
	private String getObjectSubtypeFormatted(Term term) {
		ObjectSubtype objSubtype = term.getObjectSubtype();
		if (objSubtype == null) {
			return "  ";
		} else {
			return String.format("%-2s", objSubtype.toString());
		}
	}
	
	/**
	 * frei => F; gesperrt => G; ung�ltig => U
	 * @param termStatus
	 * @return
	 */
	private String getTermStatusSerialization(TermStatus termStatus) {
		return termStatus.toString().substring(0, 1).toUpperCase();
	}

}
