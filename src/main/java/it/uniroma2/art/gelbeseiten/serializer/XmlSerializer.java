package it.uniroma2.art.gelbeseiten.serializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.uniroma2.art.gelbeseiten.exception.SerializationException;
import it.uniroma2.art.gelbeseiten.model.AppName;
import it.uniroma2.art.gelbeseiten.model.ObjectSubtype;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Relation;
import it.uniroma2.art.gelbeseiten.model.RelationType;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermEntity;
import it.uniroma2.art.gelbeseiten.model.TermFacet;
import it.uniroma2.art.gelbeseiten.model.TermNote;
import it.uniroma2.art.gelbeseiten.model.TermStatus;
import it.uniroma2.art.gelbeseiten.model.TermType;
import it.uniroma2.art.gelbeseiten.model.Zthes;

public class XmlSerializer implements Serializer {
	
	@Override
	public void serialize(Zthes zThes, File file) throws SerializationException {
		
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element zThesElement = doc.createElement(Zthes.Tag.ZTHES);
			doc.appendChild(zThesElement);
			
			List<ObjectType> objectTypes = Arrays.asList(ObjectType.T, ObjectType.R, ObjectType.N,
					ObjectType.B, ObjectType.Q, ObjectType.M, ObjectType.NA);
			
			for (ObjectType ot : objectTypes) {
				for (Term term : zThes.getTermsByObjectType(ot)) {
					if (termToExport(term)) {
						appendTerm(doc, zThesElement, zThes, term);
					}
				}
			}
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			Properties outputProps = new Properties();
			outputProps.setProperty("encoding", "UTF-8");
			outputProps.setProperty("indent", "yes");
			outputProps.setProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.setOutputProperties(outputProps);
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new FileOutputStream(file));
			transformer.transform(source, result);
			result.getOutputStream().close();
			
		} catch (ParserConfigurationException | TransformerException | IOException e) {
			throw new SerializationException(e);
		}
		
		
	}
	
	private void appendTerm(Document doc, Element zThesElement, Zthes zThes, Term term) {
		Element termElement = doc.createElement(Term.Tag.TERM);
		zThesElement.appendChild(termElement);
		
		appendTermEntityElements(doc, termElement, term);
		
		if (attributeToExport(Term.Tag.TERM_NOTE, term)) {
			List<TermNote> termNote = term.getTermNotes();
			if (termNote.size() > 0) {
				for (TermNote tn : termNote) {
					Element termNoteElement = doc.createElement(Term.Tag.TERM_NOTE);
					termElement.appendChild(termNoteElement);
					termNoteElement.setTextContent(tn.getNote());
					if (tn.getLabel() != null) {
						termNoteElement.setAttribute(TermNote.Attr.LABEL, tn.getLabel().toString());
					}
					if (tn.getVocab() != null) {
						termNoteElement.setAttribute(TermNote.Attr.VOCAB, tn.getVocab());
					}
				}
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_FACET, term)) {
			TermFacet termFacet = term.getTermFacet();
			if (termFacet != null) {
				Element termFacetElement = doc.createElement(Term.Tag.TERM_FACET);
				termElement.appendChild(termFacetElement);
				termFacetElement.setTextContent(termFacet.toString());
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_STATUS, term)) {
			TermStatus termStatus = term.getTermStatus();
			if (termStatus != null) {
				Element termStatusElement = doc.createElement(Term.Tag.TERM_STATUS);
				termElement.appendChild(termStatusElement);
				termStatusElement.setTextContent(termStatus.toString());
			}
		}
		
		if (attributeToExport(Term.Tag.OBJECT_SUBTYPE, term)) {
			ObjectSubtype objectSubtype = term.getObjectSubtype();
			if (objectSubtype != null) {
				Element objectSubtypeElement = doc.createElement(Term.Tag.OBJECT_SUBTYPE);
				termElement.appendChild(objectSubtypeElement);
				objectSubtypeElement.setTextContent(objectSubtype.toString());
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_CREATED_DATE, term)) {
			String termCreatedDate = term.getTermCreatedDate();
			if (termCreatedDate != null) {
				Element termCreatedDateElement = doc.createElement(Term.Tag.TERM_CREATED_DATE);
				termElement.appendChild(termCreatedDateElement);
				termCreatedDateElement.setTextContent(termCreatedDate);
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_CREATED_BY, term)) {
			String termCreatedBy = term.getTermCreatedBy();
			if (termCreatedBy != null) {
				Element termCreatedByElement = doc.createElement(Term.Tag.TERM_CREATED_BY);
				termElement.appendChild(termCreatedByElement);
				termCreatedByElement.setTextContent(termCreatedBy);
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_MODIFIED_DATE, term)) {
			String termModifiedDate = term.getTermModifiedBy();
			if (termModifiedDate != null) {
				Element termModifiedDateElement = doc.createElement(Term.Tag.TERM_MODIFIED_DATE);
				termElement.appendChild(termModifiedDateElement);
				termModifiedDateElement.setTextContent(termModifiedDate);
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_MODIFIED_BY, term)) {
			String termModifiedBy = term.getTermModifiedBy();
			if (termModifiedBy != null) {
				Element termModifiedByElement = doc.createElement(Term.Tag.TERM_MODIFIED_BY);
				termElement.appendChild(termModifiedByElement);
				termModifiedByElement.setTextContent(termModifiedBy);
			}
		}
		
		//Relations
		List<Relation> relation = term.getRelations();
		if (relation.size() > 0) {
			for (Relation r : relation) {
				if (!r.getTermLanguage().equals("DE")) continue;
				
				Element relationElement = doc.createElement(Term.Tag.RELATION);
				termElement.appendChild(relationElement);
				
				RelationType relationType = r.getRelationType();
				Element relationTypeElement = doc.createElement(Relation.Tag.RELATION_TYPE);
				relationElement.appendChild(relationTypeElement);
				relationTypeElement.setTextContent(relationType.toString());
				
				if (attributeToExport(Relation.Attr.WEIGHT, term)) {
					float relationWeight = r.getWeight();
					if (relationWeight != 0.0f) {
						Element relationWeightElement = doc.createElement("relationWeight");
						relationElement.appendChild(relationWeightElement);
						relationWeightElement.setTextContent(relationWeight + "");
					}
				}
				
				/*
				 * In the dtd of the output xml, unlike the dtd of the input xml,
				 * the termEntity has termDisplayStatus?, termHoursValue? elements,
				 * so instead of append a TermEntity, append the referenced Term.
				 */
				/*
				 * If input data is inconsistent and the term referenced in <relation> (through termId element) 
				 * is not defined, relatedTerm is null and it will cause a NullPointerException
				 */
				Term relatedTerm = zThes.getTermById(r.getTermId());
				if (relatedTerm == null) {
					System.out.println("Missing rel term during xml serialization: " + term.getTermId() + " -" + r.getRelationType() + "-> " + r.getTermId());
				}
				appendTermEntityElements(doc, relationElement, relatedTerm);
				
				if (attributeToExport(Relation.Tag.SOURCE_DB, relatedTerm)) {
					String sourceDb = r.getSourceDb();
					if (sourceDb != null) {
						Element sourceDbElement = doc.createElement(Relation.Tag.SOURCE_DB);
						relationElement.appendChild(sourceDbElement);
						relationElement.setTextContent(sourceDb);
					}
				}
				
			}
		}
	}
	
	private void appendTermEntityElements(Document doc, Element parentElement, Term term) {
		
		if (attributeToExport(Term.Tag.TERM_ID, term)) {
			String termId = getFixedTermId(term);
			Element termIdElement = doc.createElement(Term.Tag.TERM_ID);
			parentElement.appendChild(termIdElement);
			termIdElement.setTextContent(termId);
		}
		
		if (attributeToExport(Term.Tag.OBJECT_TYPE, term)) {
			ObjectType objectType = term.getObjectType();
			Element objectTypeElement = doc.createElement(Term.Tag.OBJECT_TYPE);
			parentElement.appendChild(objectTypeElement);
			objectTypeElement.setTextContent(objectType.toString());
		}
		
		if (attributeToExport(Term.Tag.TERM_NAME, term)) {
			String termName = term.getTermName();
			Element termNameElement = doc.createElement(Term.Tag.TERM_NAME);
			parentElement.appendChild(termNameElement);
			termNameElement.setTextContent(termName);
		}
		
		if (attributeToExport(Term.Tag.TERM_QUALIFIER, term)) {
			String termQualifier = term.getTermQualifier();
			if (termQualifier != null) {
				Element termQualifierElement = doc.createElement(Term.Tag.TERM_QUALIFIER);
				parentElement.appendChild(termQualifierElement);
				termQualifierElement.setTextContent(termQualifier);
			}
		}
		
		if (attributeToExport(Term.Tag.APP_NAME, term)) {
			AppName appName = term.getAppName();
			if (appName != null) {
				Element appNameElement = doc.createElement(Term.Tag.APP_NAME);
				parentElement.appendChild(appNameElement);
				appNameElement.setTextContent(appName.toString());
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_TYPE, term)) {
			TermType termType = term.getTermType();
			if (termType != null) {
				Element termTypeElement = doc.createElement(Term.Tag.TERM_TYPE);
				parentElement.appendChild(termTypeElement);
				termTypeElement.setTextContent(termType.toString());
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_LANGUAGE, term)) {
			String termLanguage = term.getTermLanguage();
			if (termLanguage != null) {
				Element termLanguageElement = doc.createElement(Term.Tag.TERM_LANGUAGE);
				parentElement.appendChild(termLanguageElement);
				termLanguageElement.setTextContent(termLanguage);
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_DISPLAY_STATUS, term)) {
			String termDisplayStatus = term.getTermDisplayStatus();
			if (termDisplayStatus != null) {
				Element termDisplayStatusElement = doc.createElement(Term.Tag.TERM_DISPLAY_STATUS);
				parentElement.appendChild(termDisplayStatusElement);
				termDisplayStatusElement.setTextContent(termDisplayStatus);
			}
		}
		
		if (attributeToExport(Term.Tag.TERM_HOURS_VALUE, term)) {
			String termHoursValue = term.getTermHoursValue();
			if (termHoursValue != null) {
				Element termHoursValueElement = doc.createElement(Term.Tag.TERM_HOURS_VALUE);
				parentElement.appendChild(termHoursValueElement);
				termHoursValueElement.setTextContent(termHoursValue);
			}
		}
	}
	
	/**
	 * Only DE terms with status...
	 * ObjectType B: only terms with termStatus F, G
	 * Other objectTypes:  only terms with termStatus F
	 * @param term
	 * @param objectType
	 * @return
	 */
	private boolean termToExport(Term term) {
		if (!term.getTermLanguage().equals("DE")) {
			return false;
		}
		if (term.getObjectType() == ObjectType.B) {
			return term.getTermStatus() == TermStatus.frei || term.getTermStatus() == TermStatus.gesperrt;
		} else {
			return term.getTermStatus() == TermStatus.frei;
		}
	}
	
	private boolean attributeToExport(String attribute, Term term) {
		ObjectType objectType = term.getObjectType();
		if (attribute.equals(Term.Tag.TERM_ID)) {
			return true;
		} else if (attribute.equals(Term.Tag.OBJECT_TYPE)) {
			return true;
		} else if (attribute.equals(Term.Tag.TERM_NAME)) {
			return true;
		} else if (attribute.equals(Term.Tag.TERM_QUALIFIER)) {
			return (objectType == ObjectType.T || objectType == ObjectType.R || objectType == ObjectType.B ||
					objectType == ObjectType.Q || objectType == ObjectType.N);
		} else if (attribute.equals(Term.Tag.APP_NAME)) {
			return (objectType == ObjectType.T || objectType == ObjectType.R || objectType == ObjectType.N);
		} else if (attribute.equals(Term.Tag.TERM_TYPE)) {
			return true;
		} else if (attribute.equals(Term.Tag.TERM_LANGUAGE)) {
			return true;
		} else if (attribute.equals(Term.Tag.TERM_FACET)) {
			return false;
		} else if (attribute.equals(Term.Tag.TERM_STATUS)) {
			return false;
		} else if (attribute.equals(Term.Tag.OBJECT_SUBTYPE)) {
			return false;
		} else if (attribute.equals(Term.Tag.TERM_CREATED_DATE)) {
			return false;
		} else if (attribute.equals(Term.Tag.TERM_MODIFIED_DATE)) {
			return false;
		} else if (attribute.equals(Term.Tag.TERM_MODIFIED_BY)) {
			return false;
		} else if (attribute.equals(Term.Tag.TERM_NOTE)) {
			return false;
		} else if (attribute.equals(Term.Tag.TERM_DISPLAY_STATUS)) {
			return objectType == ObjectType.B;
		} else if (attribute.equals(Term.Tag.TERM_GRAMMAR)) {
			return false;
		} else if (attribute.equals(Term.Tag.SUBJECT_GROUP)) {
			return false;
		} else if (attribute.equals(Term.Tag.SUBJECT_GROUP_NAME)) {
			return false;
		} else if (attribute.equals(Term.Tag.CLUSTER)) {
			return false;
		} else if (attribute.equals(Term.Tag.TERM_HOURS_VALUE)) {
			return objectType == ObjectType.B;
		} else if (attribute.equals(Term.Tag.TARGET_GROUP)) {
			return false;
		} else if (attribute.equals(Relation.Attr.WEIGHT)) {
			return (objectType == ObjectType.B || objectType == ObjectType.Q);
		}
		return false;
	}
	
	protected String getFixedTermId(TermEntity term) {
		if (term.getObjectType() == ObjectType.T || term.getObjectType() == ObjectType.R || term.getObjectType() == ObjectType.N) {
			return "1" + term.getTermId();
		} else if (term.getObjectType() == ObjectType.Q || term.getObjectType() == ObjectType.NA || term.getObjectType() == ObjectType.M) {
			return "9" + term.getTermId();
		} else {
			return term.getTermId();
		}
	}
	
}
