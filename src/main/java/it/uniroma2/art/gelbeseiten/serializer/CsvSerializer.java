package it.uniroma2.art.gelbeseiten.serializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.uniroma2.art.gelbeseiten.exception.SerializationException;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermStatus;
import it.uniroma2.art.gelbeseiten.model.Zthes;

public class CsvSerializer implements Serializer {

	@Override
	public void serialize(Zthes zThes, File file) throws SerializationException {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.ISO_8859_1))) {
				for (Term t : zThes.getTermsByObjectType(ObjectType.B)) {
					if (!t.getTermLanguage().equals("DE")) continue;
					
					bw.write("\"" + getTermIdFormatted(t) + "\"");
					bw.write(",");
					bw.write("\"" + t.getTermName() + "\"");
					bw.write(",");
					bw.write("\"" + t.getTermType() + "\"");
					bw.write(",");
					bw.write("\"\""); //GSB n/a
					bw.write(",");
					if (t.getTargetGroup() != null) {
						bw.write("\"" + t.getTargetGroup() + "\"");
					} else {
						bw.write("\"\"");
					}
					bw.write(",");
					bw.write("\"\""); //HRK n/a
					bw.write(",");
					if (t.getTermFacet() != null) {
						bw.write("\"" + t.getTermFacet() + "\"");
					} else {
						bw.write("\"\"");
					}
					bw.write(",");
					if (t.getSubjectGroup() != null) {
						bw.write("\"" + t.getSubjectGroup() + "\"");
					} else {
						bw.write("\"\"");
					}
					bw.write(",");
					bw.write("\"\""); //SPR n/a
					bw.write(",");
					bw.write("\"\""); //WOA n/a
					bw.write(",");
					if (t.getTermGrammar() != null) {
						bw.write("\"" + t.getTermGrammar() + "\"");
					} else {
						bw.write("\"\"");
					}
					bw.write(",");
					if (t.getObjectSubtype() != null) {
						bw.write("\"" + t.getObjectSubtype() + "\"");
					} else {
						bw.write("\"\"");
					}
					bw.write(",");
					bw.write("\"\""); //BRA n/a
					bw.write(",");
					bw.write("\"\""); //AKZ n/a
					bw.write(",");
					bw.write("\"\""); //MRK n/a
					bw.write(",");
					bw.write(getDateFormatted(t.getTermCreatedDate()));
					bw.write(",");
					bw.write(getDateFormatted(t.getTermModifiedDate()));
					bw.write(",");
					//USR n/a int
					bw.write(",");
					bw.write("\"" + getTermStatusSerialization(t.getTermStatus()) + "\"");
					bw.write(",");
					bw.write("\"\""); //TNZ n/a
					bw.write(",");
					bw.write("\"\""); //NOT n/a
					bw.write(",");
					bw.write("\"\""); //SNR n/a
					bw.write("\n");
				}
			}
		} catch (IOException e) {
			throw new SerializationException(e);
		}
	}
	
	/**
	 * leading zeroes if termID < 10000
	 * @param term
	 * @return
	 */
	private String getTermIdFormatted(Term term) {
		int idAsInt = Integer.parseInt(term.getTermId());
		return String.format("%05d", idAsInt);
	}
	
	/**
	 * From dd.MM.yyyy to yyyyMMdd
	 * @param dateString
	 * @return
	 */
	private String getDateFormatted(String dateString) {
		String formatted = "";
		if (dateString != null) {
			SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");
			try {
				Date date = parser.parse(dateString);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
				formatted = formatter.format(date);
			} catch (ParseException e) {
				System.out.println("Unable to parse date " + dateString);
				e.printStackTrace();
			}
		}
		return formatted;
	}

	/**
	 * frei => F; gesperrt => G; ung�ltig => U
	 * @param termStatus
	 * @return
	 */
	private String getTermStatusSerialization(TermStatus termStatus) {
		return termStatus.toString().substring(0, 1).toUpperCase();
	}

}
