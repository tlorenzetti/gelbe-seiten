package it.uniroma2.art.gelbeseiten.serializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import it.uniroma2.art.gelbeseiten.exception.SerializationException;
import it.uniroma2.art.gelbeseiten.model.ObjectType;
import it.uniroma2.art.gelbeseiten.model.Relation;
import it.uniroma2.art.gelbeseiten.model.Term;
import it.uniroma2.art.gelbeseiten.model.TermEntity;
import it.uniroma2.art.gelbeseiten.model.TermStatus;
import it.uniroma2.art.gelbeseiten.model.Zthes;

public class CsvShortSerializer implements Serializer {

	@Override
	public void serialize(Zthes zThes, File file) throws SerializationException {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.ISO_8859_1))) {
				for (Term t : zThes.getTermsByObjectType(ObjectType.B)) {
					if (!t.getTermLanguage().equals("DE")) continue;
					
					for (Relation r : t.getRelations()) {
						/*
						 * if relation term is not found (it should not happen) =>
						 * the termLanguage is null and it would cause a NullPointerException
						 * at the check (!r.getTermLanguage().equals("DE"))
						 * Prevent it with the following check
						 */
						if (r.getTermLanguage() == null) continue;
						
						if (!r.getTermLanguage().equals("DE")) continue;
						bw.write(getTermIdFormatted(t));
						bw.write(",");
						bw.write(getTermIdFormatted(r));
						bw.write(",");
						bw.write(r.getRelationType().toString());
						bw.write(",");
						bw.write(t.getObjectType().toString());
						bw.write(",");
						bw.write(getTermStatusSerialization(t.getTermStatus()));
						bw.write(",");
						bw.write("\"\""); //SNR n/a
						bw.write("\n");
					}
				}
			}
		} catch (IOException e) {
			throw new SerializationException(e);
		}
	}
	
	/**
	 * leading zeroes if termID < 10000
	 * @param term
	 * @return
	 */
	private String getTermIdFormatted(TermEntity term) {
		int idAsInt = Integer.parseInt(term.getTermId());
		return String.format("%05d", idAsInt);
	}
	
	/**
	 * frei => F; gesperrt => G; ung�ltig => U
	 * @param termStatus
	 * @return
	 */
	private String getTermStatusSerialization(TermStatus termStatus) {
		return termStatus.toString().substring(0, 1).toUpperCase();
	}

}
