package it.uniroma2.art.gelbeseiten.model;

public enum RelationType {

	BS(TermType.N, TermType.D),
	BF(TermType.D, TermType.N),
	BSA(TermType.N, TermType.D), //this should be A->D. Temporarily changed waiting for fixes in data .xml
	BFA(TermType.D, TermType.N), //this should be D->A. Temporarily changed waiting for fixes in data .xml
	VV(TermType.N, TermType.D),
	ABK(TermType.D, TermType.N),
	ID(TermType.D, TermType.D),
	QS(TermType.D, TermType.D),
	BK(TermType.K, TermType.D),
	KB(TermType.D, TermType.K),
	TH(TermType.D, TermType.D),
	UB(TermType.D, TermType.D),
	OBA(TermType.D, TermType.D),
	UBA(TermType.D, TermType.D),
	OBP(TermType.D, TermType.D),
	UBP(TermType.D, TermType.D),
	OBB(TermType.D, TermType.D),
	UBB(TermType.D, TermType.D),
	OBZ(TermType.D, TermType.D),
	UBZ(TermType.D, TermType.D),
	VB(TermType.D, TermType.D),
	GG(TermType.D, TermType.D),
	NB(TermType.D, TermType.D),
	PRM(TermType.D, TermType.D),
	MRK(TermType.D, TermType.D),
	BQK(TermType.D, TermType.D),
	QKB(TermType.D, TermType.D),
//	QSP(TermType.D, TermType.D), //translation from EN/FR to DE
//	ZSP(TermType.D, TermType.D), //translation from DE to EN/FR 
	QSP(TermType.D, TermType.A), //this should be D->D, but found also cases D->A
	ZSP(TermType.A, TermType.D), //this should be D->D. but found also cases A->D
	
	//To ignore
	ZTB(TermType.D, TermType.D),
	BZT(TermType.D, TermType.D);
	
	private TermType termTypeFrom;
	private TermType termTypeTo;
	
	private RelationType(TermType termTypeFrom, TermType termTypeTo) {
		this.termTypeFrom = termTypeFrom;
		this.termTypeTo = termTypeTo;
	}
	
	public TermType getTermTypeFrom() {
		return this.termTypeFrom;
	}
	
	public TermType getTermTypeTo() {
		return this.termTypeTo;
	}
	
}
