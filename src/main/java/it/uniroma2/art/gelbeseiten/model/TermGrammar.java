package it.uniroma2.art.gelbeseiten.model;

public enum TermGrammar {
	AS, AV, PP, SA, SV, V;
}
