package it.uniroma2.art.gelbeseiten.model;

public enum TermFacet {
	
	B, E, F, G, I, N, P, T, V, X;

}
