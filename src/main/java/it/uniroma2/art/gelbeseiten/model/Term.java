package it.uniroma2.art.gelbeseiten.model;

import java.util.ArrayList;
import java.util.List;

public class Term extends TermEntity {
	
	public static class Tag {
		public static final String TERM = "term";
		public static final String TERM_ID = "termId";
		public static final String OBJECT_TYPE = "objectType";
		public static final String TERM_NAME = "termName";
		public static final String TERM_QUALIFIER = "termQualifier";
		public static final String APP_NAME = "appName";
		public static final String TERM_TYPE = "termType";
		public static final String TERM_LANGUAGE = "termLanguage";
		public static final String TERM_DISPLAY_STATUS = "termDisplayStatus";
		public static final String TERM_HOURS_VALUE = "termHoursValue";
		public static final String TERM_FACET = "termFacet";
		public static final String TERM_GRAMMAR = "termGrammar";
		public static final String TERM_STATUS = "termStatus";
		public static final String OBJECT_SUBTYPE = "objectSubtype";
		public static final String SUBJECT_GROUP = "subjectGroup";
		public static final String SUBJECT_GROUP_NAME = "subjectGroupName";
		public static final String CLUSTER = "cluster";
		public static final String TARGET_GROUP = "targetGroup";
		public static final String TERM_NOTE = "termNote";
		public static final String TERM_CREATED_DATE = "termCreatedDate";
		public static final String TERM_CREATED_BY = "termCreatedBy";
		public static final String TERM_MODIFIED_DATE = "termModifiedDate";
		public static final String TERM_MODIFIED_BY = "termModifiedBy";
		public static final String RELATION = "relation";
	}

	private String termDisplayStatus; //"1" or not provided
	private String termHoursValue;
	private TermFacet termFacet;
	private TermGrammar termGrammar;
	private TermStatus termStatus;
	private ObjectSubtype objectSubtype;
	private String subjectGroup;
	private String subjectGroupName;
	private String cluster;
	private TargetGroup targetGroup;
	private List<TermNote> termNotes;
	private String termCreatedDate;
	private String termCreatedBy;
	private String termModifiedDate;
	private String termModifiedBy;
	private List<Relation> relations;
	
	public Term(String termId, ObjectType objectType, String termName) {
		super(termId, objectType, termName);
		this.termNotes = new ArrayList<TermNote>();
		this.relations = new ArrayList<Relation>();
	}


	public String getTermDisplayStatus() {
		return termDisplayStatus;
	}

	public void setTermDisplayStatus(String termDisplayStatus) {
		this.termDisplayStatus = termDisplayStatus;
	}

	public String getTermHoursValue() {
		return termHoursValue;
	}

	public void setTermHoursValue(String termHoursValue) {
		this.termHoursValue = termHoursValue;
	}

	public TermFacet getTermFacet() {
		return termFacet;
	}

	public void setTermFacet(TermFacet termFacet) {
		this.termFacet = termFacet;
	}

	public TermGrammar getTermGrammar() {
		return termGrammar;
	}

	public void setTermGrammar(TermGrammar termGrammar) {
		this.termGrammar = termGrammar;
	}

	public TermStatus getTermStatus() {
		return termStatus;
	}

	public void setTermStatus(TermStatus termStatus) {
		this.termStatus = termStatus;
	}

	public ObjectSubtype getObjectSubtype() {
		return objectSubtype;
	}

	public void setObjectSubtype(ObjectSubtype objectSubtype) {
		this.objectSubtype = objectSubtype;
	}

	public String getSubjectGroup() {
		return subjectGroup;
	}

	public void setSubjectGroup(String subjectGroup) {
		this.subjectGroup = subjectGroup;
	}

	public String getSubjectGroupName() {
		return subjectGroupName;
	}

	public void setSubjectGroupName(String subjectGroupName) {
		this.subjectGroupName = subjectGroupName;
	}

	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public TargetGroup getTargetGroup() {
		return targetGroup;
	}

	public void setTargetGroup(TargetGroup targetGroup) {
		this.targetGroup = targetGroup;
	}

	public List<TermNote> getTermNotes() {
		return termNotes;
	}

	public void setTermNotes(List<TermNote> termNotes) {
		this.termNotes = termNotes;
	}
	
	public void addTermNote(TermNote termNote) {
		this.termNotes.add(termNote);
	}

	public String getTermCreatedDate() {
		return termCreatedDate;
	}

	public void setTermCreatedDate(String termCreatedDate) {
		this.termCreatedDate = termCreatedDate;
	}

	public String getTermCreatedBy() {
		return termCreatedBy;
	}

	public void setTermCreatedBy(String termCreatedBy) {
		this.termCreatedBy = termCreatedBy;
	}

	public String getTermModifiedDate() {
		return termModifiedDate;
	}

	public void setTermModifiedDate(String termModifiedDate) {
		this.termModifiedDate = termModifiedDate;
	}

	public String getTermModifiedBy() {
		return termModifiedBy;
	}

	public void setTermModifiedBy(String termModifiedBy) {
		this.termModifiedBy = termModifiedBy;
	}

	public List<Relation> getRelations() {
		return relations;
	}
	
	public List<Relation> getRelations(RelationType type) {
		List<Relation> rels = new ArrayList<>();
		for (Relation r : this.relations) {
			if (r.getRelationType() == type) {
				rels.add(r);
			}
		}
		return rels;
	}

	public void setRelation(List<Relation> relations) {
		this.relations = relations;
	}
	
	public void addRelation(Relation relation) {
		this.relations.add(relation);
	}
	
	@Override
	public String toString() {
		return Tag.TERM_ID + " " + this.getTermId() + "\n" +
				Tag.OBJECT_TYPE + " " + this.getObjectType() + "\n" +
				Tag.TERM_NAME + " " + this.getTermName() + "\n" +
				Tag.TERM_QUALIFIER + " " + this.getTermQualifier() + "\n" +
				Tag.APP_NAME + " " + this.getAppName() + "\n" +
				Tag.TERM_TYPE + " " + this.getTermType() + "\n" +
				Tag.TERM_LANGUAGE + " " + this.getTermLanguage() + "\n" +
				Tag.TERM_DISPLAY_STATUS + " " + this.termDisplayStatus + "\n" +
				Tag.TERM_HOURS_VALUE + " " + this.termHoursValue + "\n" +
				Tag.TERM_FACET + " " + this.termFacet + "\n" +
				Tag.TERM_GRAMMAR + " " + this.termGrammar + "\n" +
				Tag.TERM_STATUS + " " + this.termStatus + "\n" +
				Tag.OBJECT_SUBTYPE + " " + this.objectSubtype + "\n" +
				Tag.SUBJECT_GROUP + " " + this.subjectGroup + "\n" +
				Tag.SUBJECT_GROUP_NAME + " " + this.subjectGroupName + "\n" +
				Tag.CLUSTER + " " + this.cluster + "\n" +
				Tag.TARGET_GROUP + " " + this.targetGroup + "\n" +
				Tag.TERM_NOTE + " " + this.termNotes + "\n" +
				Tag.TERM_CREATED_DATE + " " + this.termCreatedDate + "\n" +
				Tag.TERM_CREATED_BY + " " + this.termCreatedBy + "\n" +
				Tag.TERM_MODIFIED_DATE + " " + this.termModifiedDate + "\n" +
				Tag.TERM_MODIFIED_BY + " " + this.termModifiedBy + "\n" +
				Tag.RELATION + " " + this.relations;
	}
	
}
