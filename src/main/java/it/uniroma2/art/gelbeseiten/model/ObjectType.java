package it.uniroma2.art.gelbeseiten.model;

public enum ObjectType {
	B, //Branche (Heading)
	M, //Marke (Brand)
	N,
	NA,
	Q, //QualifiziertesKeyword (Controlled keyword)
	R, //Rubrik	(Subtopic)
	T, //Thema (Topic)
	Z;
}
