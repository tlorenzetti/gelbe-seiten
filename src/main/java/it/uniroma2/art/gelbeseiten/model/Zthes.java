package it.uniroma2.art.gelbeseiten.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class Zthes {
	
	public static class Tag {
		public static final String ZTHES = "Zthes";
	}
	
	private Map<String, Term> termMap; //termId - term
	private Multimap<RelationType, Term> relationMap; //relationType - [termWithThatRelation]
	private Multimap<ObjectType, Term> objectTypeMap; //objectType - Term
	
	public Zthes() {
		termMap = new HashMap<String, Term>();
		relationMap = HashMultimap.create();
		objectTypeMap = HashMultimap.create();
	}
	
	public Collection<Term> getTerms() {
		return termMap.values();
	}
	
	public Term getTermById(String termId) {
		return termMap.get(termId);
	}
	
	public void addTerm(Term term) {
		termMap.put(term.getTermId(), term);
		objectTypeMap.put(term.getObjectType(), term);
		//Add the termId-relatedTermId to the map
		for (Relation relation : term.getRelations()) {//for each relation of the term
			RelationType relationType = relation.getRelationType();
			relationMap.put(relationType, term);
		}
	}
	
	public Collection<Term> getTermsWithRelation(RelationType relationType) {
		return relationMap.get(relationType);
	}
	
	public Collection<Term> getTermsByObjectType(ObjectType objectType) {
		return objectTypeMap.get(objectType);
	}
	
	/**
	 * Returns the pivot descriptor term for the given term.
	 * A pivot term is the term which will be used to create a skos:Concept.
	 * A pivot term by default has german language.
	 * If the german language is not available for the given term, return the english term.
	 * If also the english term is not available, returns the french term.
	 * @param term
	 * @return
	 */
	public TermEntity getPivotTerm(TermEntity entity) {
		Term term = getTermById(entity.getTermId());
		if (term == null) { //entity is not defined as term
			if (entity.getTermType() == TermType.D) {
				return entity;
			}
		}
		if (getNormalizedTermLanguage(term).equals(Locale.GERMAN.getLanguage()) && 
				(term.getTermType() == TermType.D || term.getTermType() == TermType.A)) {
			//the given term is already a german descriptor
			return term;
		}
		

		/* ================================
		 * Looks for german descriptor term:
		 * ===============================*/
		/*
		 * If its german but not a descriptor, look for the descriptor
		 * BS is the relation that foes from non-descriptor to descriptor
		 */
		if (term.getTermType() == TermType.N) {
			Term germanDescriptor = null;
			Term englishDescriptor = null;
			Term frenchDescriptor = null;
			for (Relation rel : term.getRelations()) {
				if (rel.getRelationType() == RelationType.BS) {
					if (getNormalizedTermLanguage(rel).equals(Locale.GERMAN.getLanguage()) && rel.getTermType() == TermType.D) {
						germanDescriptor = getTermById(rel.getTermId());
					} else if (getNormalizedTermLanguage(rel).equals(Locale.ENGLISH.getLanguage()) && rel.getTermType() == TermType.D) {
						englishDescriptor = getTermById(rel.getTermId());
					} else if (getNormalizedTermLanguage(rel).equals(Locale.FRENCH.getLanguage()) && rel.getTermType() == TermType.D) {
						frenchDescriptor = getTermById(rel.getTermId());
					}
				}
			}
			if (germanDescriptor != null) {
				return germanDescriptor;
			} else if (englishDescriptor != null) {
				return englishDescriptor;
			} else if (frenchDescriptor != null) {
				return frenchDescriptor;
			}
		} else { //descriptor not german (otherwise, if german, the term would be returned before)
			/*
			 * Looks for QSP relations from the term.
			 * QSP is the relation that goes from foreign term to german term
			 */
			for (Relation rel : term.getRelations()) {
				if (rel.getRelationType() == RelationType.QSP) {
					if (getNormalizedTermLanguage(rel).equals(Locale.GERMAN.getLanguage()) && rel.getTermType() == TermType.D) {
						return getTermById(rel.getTermId());
					}
				}
			}
			/* =================================
			 * Looks for english descriptor term:
			 * ================================*/
			for (Relation rel : term.getRelations()) {
				//ZSP is the relation that goes from german to foreign term
				if (rel.getRelationType() == RelationType.ZSP) {
					if (getNormalizedTermLanguage(rel).equals(Locale.ENGLISH.getLanguage()) && rel.getTermType() == TermType.D) {
						return getTermById(rel.getTermId());
					}
				}
			}
			//there is no german or english descriptor (it should be french) => return the term if it is a descriptor
			if (term.getTermType() == TermType.D) {
				return term;
			}
		}
		
		return null; //not found any Descriptor pivot term
	}
	
	private String getNormalizedTermLanguage(TermEntity entity) {
		return new Locale(entity.getTermLanguage()).getLanguage();
	}
	
}
