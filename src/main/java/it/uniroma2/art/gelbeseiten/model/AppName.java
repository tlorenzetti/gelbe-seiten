package it.uniroma2.art.gelbeseiten.model;

public enum AppName {
	
	GS_ONLINE("GS-Online"),
	GS_IPHONE("GS-iPhone"),
	GS_NAHE("GS-N�he");
	
	private String value;
	
	private AppName(String value) {
		this.value = value;
	}
	
	public static AppName fromString(String arg0) {
		for (AppName v : values()) {
			if (v.value.equals(arg0)) {
				return v;
			}
		}
		throw new IllegalArgumentException();
	}
	
	@Override
    public String toString() {
        return this.value;
    }

}
