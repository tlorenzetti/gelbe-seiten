package it.uniroma2.art.gelbeseiten.model;

public enum TermType {
	D,	//Preferred term (also known as a descriptor)
	N,	//Non-descriptor: that is, a non-preferred term.
	A,	//Non-descriptor which points to [2,n] preferred terms to choose from.
	K;	//Non-descriptor which is a complex term and points to [2,n] preferred terms which areto be used in combination
	
	public boolean subsumes(TermType termType) {
		if (this == termType) {
			return true;
		}
		if (this == TermType.A) {
			return termType == TermType.N;
		} else if (this == TermType.K) {
			return termType == TermType.A || termType == TermType.N;
		}
		return false;
	}
	
	public boolean isSubsumedBy(TermType termType) {
		if (this == termType) {
			return true;
		}
		if (this == TermType.N) {
			return termType == TermType.A || this == TermType.K;
		} else if (this == TermType.A) {
			return termType == TermType.K;
		}
		return false;
	}
}
