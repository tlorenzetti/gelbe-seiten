package it.uniroma2.art.gelbeseiten.model;

public abstract class TermEntity {
	
	private String termId;
	private ObjectType objectType;
	private String termName;
	private String termQualifier;
	private AppName appName;
	private TermType termType;
	private String termLanguage;
	
	public TermEntity(String termId, ObjectType objectType, String termName) {
		this.termId = termId;
		this.objectType = objectType;
		this.termName = termName;
	}
	
	/**
	 * Returns a 6-digits id omitting leading 1 or 9 when the id is 7-digits
	 * @return
	 */
	public String getTermId() {
		return getTermId(false);
	}
	
	/**
	 * Returns the id of the term. If {@code original} is {@code true} returns the id as it is, otherwise
	 * returns it as 6-digit id
	 * @param original
	 * @return
	 */
	public String getTermId(boolean original) {
		if (original) {
			return termId;
		} else {
			String termId6Digit = "000000" + termId; //leading 0 if termId length is < 6
			return termId6Digit.substring(termId6Digit.length()-6);
		}
	}

	public void setTermId(String termId) {
		this.termId = termId;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	public String getTermQualifier() {
		return termQualifier;
	}

	public void setTermQualifier(String termQualifier) {
		this.termQualifier = termQualifier;
	}

	public AppName getAppName() {
		return appName;
	}

	public void setAppName(AppName appName) {
		this.appName = appName;
	}
	
	public TermType getTermType() {
		return termType;
	}

	public void setTermType(TermType termType) {
		this.termType = termType;
	}

	public String getTermLanguage() {
		return termLanguage;
	}

	public void setTermLanguage(String termLanguage) {
		this.termLanguage = termLanguage;
	}

}