package it.uniroma2.art.gelbeseiten.model;

public enum ObjectSubtype {

	BG,	BQ, DG, DW, DX, G, K, Q, V, VG, VQ, W, X, XX, Z, Z1, ZB;
	
}
