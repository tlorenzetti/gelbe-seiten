package it.uniroma2.art.gelbeseiten.model;

public class TermNote {
	
	public enum NoteLabel { Definition, History; }
	
	public static class Attr {
		public static final String LABEL = "label";
		public static final String VOCAB = "vocab";
	}

	private String note; //content
	private NoteLabel label; //attr
	private String vocab; //attr
	
	public TermNote(String note) {
		this.note = note;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public NoteLabel getLabel() {
		return label;
	}

	public void setLabel(NoteLabel label) {
		this.label = label;
	}

	public String getVocab() {
		return vocab;
	}

	public void setVocab(String vocab) {
		this.vocab = vocab;
	}
	
	@Override
	public String toString() {
		return this.note + "\n" +
				Attr.LABEL + " " + this.label + "\n" +
				Attr.VOCAB + " " + this.vocab;
	}
	
}
